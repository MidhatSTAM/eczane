"""lita URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from eczane import views as eczane_views

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^$', eczane_views.homepage, name='homepage'),
	
	url(r'^pharmacy/$', eczane_views.pharmacy_list, name="pharmacy_table"),
	url(r'^pharmacy/(\d+)/$', eczane_views.pharmacy_list),
	url(r'^pharmacy/change-show-count/$', eczane_views.pharmacies_change_show_count),
	url(r'^pharmacy/add/$', eczane_views.pharmacy_add),
	url(r'^pharmacy/pharmacy-delete/$', eczane_views.pharmacy_delete),
	url(r'^pharmacy/search/$', eczane_views.pharmacy_search),
	url(r'^pharmacy/search/(\d+)/$', eczane_views.pharmacy_search),
	url(r'^pharmacy/address/edit/$', eczane_views.pharmacy_address_edit),
	url(r'^pharmacy/address/cancel/$', eczane_views.pharmacy_address_cancel),
	url(r'^pharmacy/contact/edit/$', eczane_views.pharmacy_contact_edit),
	url(r'^pharmacy/contact/cancel/$', eczane_views.pharmacy_contact_cancel),
	
	url(r'^product-brands/$', eczane_views.product_brands_list),
	url(r'^product-brands/(\d+)/$', eczane_views.product_brands_list),
	url(r'^product-brands/change-show-count/$', eczane_views.product_brands_change_show_count),
	url(r'^product-brands/add/$', eczane_views.product_brands_add),
	url(r'^product-brands/product-brands-delete/$', eczane_views.product_brands_delete),
	url(r'^product-brands/search/$', eczane_views.product_brands_search),
	url(r'^product-brands/search/(\d+)/$', eczane_views.product_brands_search),
	
	url(r'^product/$', eczane_views.product_list, name="all_products"),
	url(r'^product/(\d+)/$', eczane_views.product_list, name="product_list"),
	url(r'^product/change-show-count/$', eczane_views.product_change_show_count),
	url(r'^product/add/$', eczane_views.product_add),
	url(r'^product/product-delete/$', eczane_views.product_delete),
	url(r'^product/search-ajax/$', eczane_views.product_ajax),
	url(r'^product/search-ajax-edit/$', eczane_views.product_ajax_edit),
	url(r'^product/search/$', eczane_views.product_search),
	url(r'^product/search/(\d+)/$', eczane_views.product_search),
	
	url(r'^city/$', eczane_views.city_list),
	url(r'^city/(\d+)/$', eczane_views.city_list),
	url(r'^city/change-show-count/$', eczane_views.city_change_show_count),
	url(r'^city/add/$', eczane_views.city_add),
	url(r'^city/city-delete/$', eczane_views.city_delete),
	
	url(r'^county/$', eczane_views.county_list),
	url(r'^county/(\d+)/$', eczane_views.county_list),
	url(r'^county/change-show-count/$', eczane_views.county_change_show_count),
	url(r'^county/add/$', eczane_views.county_add),
	url(r'^county/county-delete/$', eczane_views.county_delete),
	
	url(r'^district/$', eczane_views.district_list),
	url(r'^district/(\d+)/$', eczane_views.district_list),
	url(r'^district/change-show-count/$', eczane_views.district_change_show_count),
	url(r'^district/add/$', eczane_views.district_add),
	url(r'^district/district-delete/$', eczane_views.district_delete),
	url(r'^ajax/load_counties/$', eczane_views.load_counties, name="ajax_load_counties"),
	
	url(r'^person/$', eczane_views.person_list),
	url(r'^person/(\d+)/$', eczane_views.person_list),
	url(r'^person/change-show-count/$', eczane_views.person_change_show_count),
	url(r'^person/add/$', eczane_views.person_add),
	url(r'^person/person-delete/$', eczane_views.person_delete),
	url(r'^person/search/$', eczane_views.person_search),
	url(r'^person/search/(\d+)/$', eczane_views.person_search),
	url(r'^person/address/edit/$', eczane_views.person_address_edit),
	url(r'^person/address/cancel/$', eczane_views.person_address_cancel),
	url(r'^person/contact/edit/$', eczane_views.person_contact_edit),
	url(r'^person/contact/cancel/$', eczane_views.person_contact_cancel),
	
	url(r'^address/$', eczane_views.address_list),
	url(r'^address/(\d+)/$', eczane_views.address_list),
	url(r'^address/change-show-count/$', eczane_views.address_change_show_count),
	url(r'^address/add/$', eczane_views.address_add),
	url(r'^address/address-delete/$', eczane_views.address_delete),
	url(r'^ajax/address_load_counties/$', eczane_views.address_load_counties, name="ajax_address_load_counties"),
	url(r'^address/ajax_address_load_districts/$', eczane_views.address_load_districts,
		name="ajax_address_load_districts"),
	
	url(r'^contact-types/$', eczane_views.contact_types_list),
	url(r'^contact-types/(\d+)/$', eczane_views.contact_types_list),
	url(r'^contact-types/change-show-count/$', eczane_views.contact_types_change_show_count),
	url(r'^contact-types/add/$', eczane_views.contact_types_add),
	url(r'^contact-types/contact-types-delete/$', eczane_views.contact_types_delete),
	
	url(r'^contact-info/$', eczane_views.contact_info_list),
	url(r'^contact-info/(\d+)/$', eczane_views.contact_info_list),
	url(r'^contact-info/change-show-count/$', eczane_views.contact_info_change_show_count),
	url(r'^contact-info/add/$', eczane_views.contact_info_add),
	url(r'^contact-info/contact-info-delete/$', eczane_views.contact_info_delete),
	
	url(r'^product-buy/$', eczane_views.product_buy_list, name='product_buy_table'),
	url(r'^product-buy/(\d+)/$', eczane_views.product_buy_list),
	url(r'^product-buy/change-show-count/$', eczane_views.product_buy_change_show_count),
	url(r'^product-buy/add/$', eczane_views.product_buy_add),
	url(r'^product-buy/product-buy-delete/$', eczane_views.product_buy_delete),
	url(r'^product-buy/search-ajax/$', eczane_views.product_buy_ajax),
	url(r'^product-buy/search-ajax-edit/$', eczane_views.product_buy_ajax_edit),
	url(r'^product-buy/search/$', eczane_views.product_buy_search),
	url(r'^product-buy/search/(\d+)/$', eczane_views.product_buy_search),
	
	url(r'^product-sale/$', eczane_views.product_sale_list, name='product_sale_table'),
	url(r'^product-sale/(\d+)/$', eczane_views.product_sale_list),
	url(r'^product-sale/change-show-count/$', eczane_views.product_sale_change_show_count),
	url(r'^product-sale/add/$', eczane_views.product_sale_add),
	url(r'^product-sale/product-sale-delete/$', eczane_views.product_sale_delete),
	url(r'^product-sale/search-ajax/$', eczane_views.product_sale_ajax),
	url(r'^product-sale/search-ajax-edit/$', eczane_views.product_sale_ajax_edit),
	url(r'^product-sale/search-ajax-pharmacy/$', eczane_views.product_sale_ajax_pharmacy),
	url(r'^product-sale/search-ajax-pharmacy-edit/$', eczane_views.product_sale_ajax_edit_pharmacy),
	url(r'^product-sale/search/$', eczane_views.product_sale_search),
	url(r'^product-sale/search/(\d+)/$', eczane_views.product_sale_search),
	
	url(r'^user-license/$', eczane_views.user_license_list, name='user_license_table'),
	url(r'^user-license/(\d+)/$', eczane_views.user_license_list),
	url(r'^user-license/change-show-count/$', eczane_views.user_license_change_show_count),
	url(r'^user-license/add/$', eczane_views.user_license_add),
	url(r'^user-license/user-license-delete/$', eczane_views.user_license_delete),
	url(r'^user-license/search-ajax/$', eczane_views.user_license_ajax),
	url(r'^user-license/search-ajax-edit/$', eczane_views.user_license_ajax_edit),
	url(r'^user-license/search-ajax-pharmacy/$', eczane_views.user_license_ajax_pharmacy),
	url(r'^user-license/search-ajax-pharmacy-edit/$', eczane_views.user_license_ajax_edit_pharmacy),
	url(r'^user-license/search/$', eczane_views.user_license_search),
	url(r'^user-license/search/(\d+)/$', eczane_views.user_license_search),
	
	url(r'^login/$', auth_views.login, name='login'),
	url(r'^login/submit/$', eczane_views.user_login),
	url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),

]
