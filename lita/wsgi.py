"""
WSGI config for Lita project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys
import site
import django

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/home/boran/lita/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH

sys.path.append('/home/boran/lita/local/lib/python2.7/site-packages')
sys.path.append('/home/boran/lita/bin')
sys.path.append('/home/boran/lita/bin/lita')
sys.path.append('/home/boran/lita/bin/lita/static')
sys.path.append('/home/boran/lita/bin/lita/lita')

# Activate your virtual env
activate_env=("/home/boran/lita/bin/activate_this.py")
#execfile(activate_env, dict(__file__=activate_env))

from django.core.wsgi import get_wsgi_application

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lita.settings")
os.environ['DJANGO_SETTINGS_MODULE'] = 'lita.settings'
application = get_wsgi_application()
