var total_users = document.getElementById('total_users');
var total_users_value = total_users.value;

var total_pharmacies = document.getElementById('total_pharmacies');
var total_pharmacies_value = total_pharmacies.value;

var total_products = document.getElementById('total_products');
var total_products_value = total_products.value;

var total_product_brands = document.getElementById('total_product_brands');
var total_product_brands_value = total_product_brands.value;

var Dashboard = function () {

    var _RoundedProgressChart = function (element, radius, border, color, iconClass, textTitle, end, textAverage) {
        if (typeof d3 == 'undefined') {
            console.warn('Warning - d3.min.js is not loaded.');
            return;
        }

        // Initialize chart only if element exsists in the DOM
        if ($(element).length > 0) {


            // Basic setup
            // ------------------------------

            // Main variables
            var d3Container = d3.select(element),
                startPercent = 0,
                iconSize = 15,
                endPercent = end,
                twoPi = Math.PI * 2,
                formatPercent = d3.format('.0'),
                boxSize = radius * 2;

            // Values count
            var count = Math.abs((endPercent - startPercent));

            // Values step
            var step = endPercent < startPercent ? -1 : 1;


            // Create chart
            // ------------------------------

            // Add SVG element
            var container = d3Container.append('svg');

            // Add SVG group
            var svg = container
                .attr('width', boxSize)
                .attr('height', boxSize)
                .attr('class', 'pull-right')
                .append('g')
                .attr('transform', 'translate(' + (boxSize / 2) + ',' + (boxSize / 2) + ')');


            // Construct chart layout
            // ------------------------------

            // Arc
            var arc = d3.svg.arc()
                .startAngle(0)
                .innerRadius(radius)
                .outerRadius(radius - border);


            //
            // Append chart elements
            //

            // Paths
            // ------------------------------

            // Background path
            svg.append('path')
                .attr('class', 'd3-progress-background')
                .attr('d', arc.endAngle(twoPi))
                .style('fill', '#eee');

            // Foreground path
            var foreground = svg.append('path')
                .attr('class', 'd3-progress-foreground')
                .attr('filter', 'url(#blur)')
                .style('fill', color)
                .style('stroke', color);

            // Front path
            var front = svg.append('path')
                .attr('class', 'd3-progress-front')
                .style('fill', color)
                .style('fill-opacity', 1);


            // Text
            // ------------------------------

            var text = d3.select(element)
                .append('span')
                .attr('style', 'font-size: 1.2rem;')

            // Percentage text value
            var numberText = d3.select(element)
                .append('span')
                .attr('class', 'pt-1 mt-2 mb-1 mr-1')
                .attr('style', 'font-size: 1.2rem;');

            // Icon
            d3.select(element)
                .append('i')
                .attr('class', iconClass + ' pull-right')
                .attr('style', 'top: 1.3rem;' + 'right: 1.4rem;' + 'font-size: 2rem;' + 'position: absolute;');

            // Title

            // Subtitle
            d3.select(element)
                .append('span')
                .attr('class', 'font-size-sm text-muted mb-3');


            // Animation
            // ------------------------------

            // Animate path
            function updateProgress(progress) {
                foreground.attr('d', arc.endAngle(twoPi * progress));
                front.attr('d', arc.endAngle(twoPi * progress));
                text.text(textTitle);
                numberText.text(formatPercent(progress));
            }

            // Animate text
            var progress = startPercent;
            (function loops() {
                updateProgress(progress);
                if (count > 0) {
                    count--;
                    progress += step;
                    setInterval(loops, 0)
                }
            })();
        }
    };

    return {
        initCharts: function () {
            _RoundedProgressChart('#total-users-progress', 38, 2, '#FFF', 'icon-user text-white-400', 'Toplam kullanıcı sayısı: ', total_users_value);
            _RoundedProgressChart('#total-pharmacies-progress', 38, 2, '#FFF', 'icon-office text-white-400', 'Toplam eczane sayısı: ', total_pharmacies_value);
            _RoundedProgressChart('#total-products-progress', 38, 2, '#FFF', 'icon-box text-white-400', 'Toplam ürün sayısı: ', total_products_value);
            _RoundedProgressChart('#total-product-brands-progress', 38, 2, '#FFF', 'icon-price-tag3 text-white-400', 'Toplam marka sayısı: ', total_product_brands_value);
        }
    }
}();

document.addEventListener('DOMContentLoaded', function () {
    Dashboard.initCharts();
});