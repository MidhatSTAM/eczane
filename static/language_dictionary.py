# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django import template
from ecela import auth_settings
register = template.Library()


@register.simple_tag
def general_schedule(lang=None):
	if lang == 'en':
		content = "Schedule"
	elif lang == 'tr':
		content = "Görevler"
	elif lang == 'bs':
		content = "Raspored"
	elif lang == 'ru':
		content = "Расписание"
	else:
		content = "Schedule"
	return content


@register.simple_tag
def general_not_specified(lang=None):
	if lang == 'en':
		content = "Not specified"
	elif lang == 'tr':
		content = "Belirtilmemiş"
	elif lang == 'bs':
		content = "Neodređeno"
	elif lang == 'ru':
		content = "Не указано"
	else:
		content = "Not specified"
	return content


@register.simple_tag
def general_navigation(lang=None):
	if lang == 'en':
		content = "Navigation"
	elif lang == 'tr':
		content = "Bölümler"
	elif lang == 'bs':
		content = "Navigacija"
	elif lang == 'ru':
		content = "Разделы"
	else:
		content = "Navigation"
	return content


@register.simple_tag
def general_main_menu(lang=None):
	if lang == 'en':
		content = "Main menu"
	elif lang == 'tr':
		content = "Ana menü"
	elif lang == 'bs':
		content = "Glavni meni"
	elif lang == 'ru':
		content = "Главное меню"
	else:
		content = "Main menu"
	return content


@register.simple_tag
def general_dashboard(lang=None):
	if lang == 'en':
		content = "Dashboard"
	elif lang == 'tr':
		content = "Anasayfa"
	elif lang == 'bs':
		content = "Kontrolna ploča"
	elif lang == 'ru':
		content = "Панель управления"
	else:
		content = "Dashboard"
	return content


@register.simple_tag
def general_company(lang=None):
	if lang == 'en':
		content = "Company"
	elif lang == 'tr':
		content = "Şirketim"
	elif lang == 'bs':
		content = "Firma"
	elif lang == 'ru':
		content = "Компания"
	else:
		content = "Company"
	return content


@register.simple_tag
def general_branches(lang=None):
	if lang == 'en':
		content = "Branches"
	elif lang == 'tr':
		content = "Şubeler"
	elif lang == 'bs':
		content = "Filijala"
	elif lang == 'ru':
		content = "Филиалы"
	else:
		content = "Branches"
	return content


@register.simple_tag
def general_username(lang=None):
	if lang == 'en':
		content = "Username"
	elif lang == 'tr':
		content = "Kullanıcı adı"
	elif lang == 'bs':
		content = "Korisničko ime"
	elif lang == 'ru':
		content = "Имя пользователя"
	else:
		content = "Username"
	return content


@register.simple_tag
def general_email(lang=None):
	if lang == 'en':
		content = "E-mail"
	elif lang == 'tr':
		content = "E-mail"
	elif lang == 'bs':
		content = "E-mail"
	elif lang == 'ru':
		content = "E-mail"
	else:
		content = "E-mail"
	return content


@register.simple_tag
def general_password(lang=None):
	if lang == 'en':
		content = "Password"
	elif lang == 'tr':
		content = "Parola"
	elif lang == 'bs':
		content = "Šifra"
	elif lang == 'ru':
		content = "Пароль"
	else:
		content = "Password"
	return content


@register.simple_tag
def general_login_to_your_account(lang=None):
	if lang == 'en':
		content = "Login to your account"
	elif lang == 'tr':
		content = "Hesabınıza giriş yapın"
	elif lang == 'bs':
		content = "Prijavi se"
	elif lang == 'ru':
		content = "Войдите в аккаунт"
	else:
		content = "Login to your account"
	return content


@register.simple_tag
def general_logout(lang=None):
	if lang == 'en':
		content = "Logout"
	elif lang == 'tr':
		content = "Çıkış yap"
	elif lang == 'bs':
		content = "Odjava"
	elif lang == 'ru':
		content = "Выйти"
	else:
		content = "Logout"
	return content


@register.simple_tag
def general_my_profile(lang=None):
	if lang == 'en':
		content = "My profile"
	elif lang == 'tr':
		content = "Profilim"
	elif lang == 'bs':
		content = "Moj profil"
	elif lang == 'ru':
		content = "Мой профиль"
	else:
		content = "My profile"
	return content


@register.simple_tag
def general_messages(lang=None):
	if lang == 'en':
		content = "Messages"
	elif lang == 'tr':
		content = "Mesajlar"
	elif lang == 'bs':
		content = "Poruke"
	elif lang == 'ru':
		content = "Сообщения"
	else:
		content = "Messages"
	return content


@register.simple_tag
def general_remember_me(lang=None):
	if lang == 'en':
		content = "Remember me"
	elif lang == 'tr':
		content = "Beni hatırla"
	elif lang == 'bs':
		content = "Zapamti me"
	elif lang == 'ru':
		content = "Запомнить меня"
	else:
		content = "Remember me"
	return content


@register.simple_tag
def general_forgot_pass_ques(lang=None):
	if lang == 'en':
		content = "Forgot your password"
	elif lang == 'tr':
		content = "Parolanızı unuttunuz mu?"
	elif lang == 'bs':
		content = "Izgubljena šifra"
	elif lang == 'ru':
		content = "Забыли свой пароль?"
	else:
		content = "Forgot your password"
	return content


@register.simple_tag
def general_sign_in(lang=None):
	if lang == 'en':
		content = "Sign in"
	elif lang == 'tr':
		content = "Giriş yap"
	elif lang == 'bs':
		content = "Prijavi se"
	elif lang == 'ru':
		content = "Войти"
	else:
		content = "Sign in"
	return content


@register.simple_tag
def general_dont_have_account_quest(lang=None):
	if lang == 'en':
		content = "Don't have account?"
	elif lang == 'tr':
		content = "Hesabınız yok mu?"
	elif lang == 'bs':
		content = "Nemate račun?"
	elif lang == 'ru':
		content = "У вас нет аккаунта?"
	else:
		content = "Don't have account?"
	return content


@register.simple_tag
def general_sign_up(lang=None):
	if lang == 'en':
		content = "Sign up"
	elif lang == 'tr':
		content = "Kayıt ol"
	elif lang == 'bs':
		content = "Izradi račun"
	elif lang == 'ru':
		content = "Зарегистрироваться"
	else:
		content = "Sign up"
	return content


@register.simple_tag
def general_status_online(lang=None):
	if lang == 'en':
		content = "Online"
	elif lang == 'tr':
		content = "Çevrim içi"
	elif lang == 'bs':
		content = "Online"
	elif lang == 'ru':
		content = "В сети"
	else:
		content = "Online"
	return content


@register.simple_tag
def general_status_offline(lang=None):
	if lang == 'en':
		content = "Offline"
	elif lang == 'tr':
		content = "Çevrim dışı"
	elif lang == 'bs':
		content = "Offline"
	elif lang == 'ru':
		content = "Не в сети"
	else:
		content = "Offline"
	return content


@register.simple_tag
def general_status_away(lang=None):
	if lang == 'en':
		content = "Away"
	elif lang == 'tr':
		content = "Uzakta"
	elif lang == 'bs':
		content = "Vani"
	elif lang == 'ru':
		content = "Отсутствую"
	else:
		content = "Away"
	return content


@register.simple_tag
def general_account_security(lang=None):
	if lang == 'en':
		content = "Account security"
	elif lang == 'tr':
		content = "Hesap güvenliği"
	elif lang == 'bs':
		content = "Sigurnost računa"
	elif lang == 'ru':
		content = "Защита аккаунта"
	else:
		content = "Account security"
	return content


@register.simple_tag
def general_analytics(lang=None):
	if lang == 'en':
		content = "Analytics"
	elif lang == 'tr':
		content = "İstatistik"
	elif lang == 'bs':
		content = "Statistika"
	elif lang == 'ru':
		content = "Статистика"
	else:
		content = "Analytics"
	return content


@register.simple_tag
def general_accessibility(lang=None):
	if lang == 'en':
		content = "Accessibility"
	elif lang == 'tr':
		content = "Erişim"
	elif lang == 'bs':
		content = "Pristupačnost"
	elif lang == 'ru':
		content = "Доступ"
	else:
		content = "Accessibility"
	return content


@register.simple_tag
def general_support(lang=None):
	if lang == 'en':
		content = "Support"
	elif lang == 'tr':
		content = "Destek"
	elif lang == 'bs':
		content = "Podrška"
	elif lang == 'ru':
		content = "Поддержка"
	else:
		content = "Support"
	return content


@register.simple_tag
def general_all_settings(lang=None):
	if lang == 'en':
		content = "All Settings"
	elif lang == 'tr':
		content = "Tüm ayarlar"
	elif lang == 'bs':
		content = "Sve postavke"
	elif lang == 'ru':
		content = "Все Настройки"
	else:
		content = "All Settings"
	return content


@register.simple_tag
def general_settings(lang=None):
	if lang == 'en':
		content = "Settings"
	elif lang == 'tr':
		content = "Ayarlar"
	elif lang == 'bs':
		content = "Postavke"
	elif lang == 'ru':
		content = "Настройки"
	else:
		content = "Settings"
	return content


@register.simple_tag
def general_connections(lang=None):
	if lang == 'en':
		content = "Connections"
	elif lang == 'tr':
		content = "Bağlantılar"
	elif lang == 'bs':
		content = "Veze"
	elif lang == 'ru':
		content = "Соединения"
	else:
		content = "Connections"
	return content


@register.simple_tag
def general_invoices(lang=None):
	if lang == 'en':
		content = "Invoices"
	elif lang == 'tr':
		content = "İnvoyslar"
	elif lang == 'bs':
		content = "Fakture"
	elif lang == 'ru':
		content = "Счета"
	else:
		content = "Invoices"
	return content


@register.simple_tag
def profile_user_profile(lang=None):
	if lang == 'en':
		content = "User profile"
	elif lang == 'tr':
		content = "Kullanıcı profili"
	elif lang == 'bs':
		content = "Korisnički profil"
	elif lang == 'ru':
		content = "Профиль пользователя"
	else:
		content = "User profile"
	return content


@register.simple_tag
def profile_my_user_profile(lang=None):
	if lang == 'en':
		content = "My profile"
	elif lang == 'tr':
		content = "Kullanıcı profilim"
	elif lang == 'bs':
		content = "Moj profil"
	elif lang == 'ru':
		content = "Мой профиль"
	else:
		content = "My profile"
	return content


@register.simple_tag
def profile_profile_overview(lang=None):
	if lang == 'en':
		content = "Profile overview"
	elif lang == 'tr':
		content = "Profil bakışı"
	elif lang == 'bs':
		content = "Pregled profila"
	elif lang == 'ru':
		content = "Обзор профиля"
	else:
		content = "Profile overview"
	return content


@register.simple_tag
def profile_personal_information(lang=None):
	if lang == 'en':
		content = "Personal information"
	elif lang == 'tr':
		content = "Şahıs bilgilerim"
	elif lang == 'bs':
		content = "Korisničke informacije"
	elif lang == 'ru':
		content = "Личная информация"
	else:
		content = "Personal information"
	return content


@register.simple_tag
def profile_address_information(lang=None):
	if lang == 'en':
		content = "Address information"
	elif lang == 'tr':
		content = "Adres bilgilerim"
	elif lang == 'bs':
		content = "Informacije o adresi"
	elif lang == 'ru':
		content = "Адресная информация"
	else:
		content = "Address information"
	return content


@register.simple_tag
def profile_contact_information(lang=None):
	if lang == 'en':
		content = "Contact information"
	elif lang == 'tr':
		content = "İletişim bilgilerim"
	elif lang == 'bs':
		content = "Kontakt informacije"
	elif lang == 'ru':
		content = "Информация для связи"
	else:
		content = "Contact information"
	return content


@register.simple_tag
def profile_date_of_birth(lang=None):
	if lang == 'en':
		content = "Date of birth"
	elif lang == 'tr':
		content = "Doğum tarihi"
	elif lang == 'bs':
		content = "Datum rođenja"
	elif lang == 'ru':
		content = "Дата рождения"
	else:
		content = "Date of birth"
	return content


@register.simple_tag
def profile_country(lang=None):
	if lang == 'en':
		content = "Country"
	elif lang == 'tr':
		content = "Ülke"
	elif lang == 'bs':
		content = "Država"
	elif lang == 'ru':
		content = "Страна"
	else:
		content = "Country"
	return content


@register.simple_tag
def profile_province(lang=None):
	if lang == 'en':
		content = "Province"
	elif lang == 'tr':
		content = "İl"
	elif lang == 'bs':
		content = "Regija"
	elif lang == 'ru':
		content = "Регион"
	else:
		content = "Province"
	return content


@register.simple_tag
def profile_city(lang=None):
	if lang == 'en':
		content = "City"
	elif lang == 'tr':
		content = "Şehir"
	elif lang == 'bs':
		content = "Grad"
	elif lang == 'ru':
		content = "Город"
	else:
		content = "City"
	return content


@register.simple_tag
def profile_address(lang=None):
	if lang == 'en':
		content = "Address"
	elif lang == 'tr':
		content = "Adres"
	elif lang == 'bs':
		content = "Adresa"
	elif lang == 'ru':
		content = "Адрес"
	else:
		content = "Address"
	return content


@register.simple_tag
def profile_mobile(lang=None):
	if lang == 'en':
		content = "Mobile phone number"
	elif lang == 'tr':
		content = "Cep tel. numarası"
	elif lang == 'bs':
		content = "Mobilni telefon"
	elif lang == 'ru':
		content = "Номер мобильного"
	else:
		content = "Mobile phone number"
	return content


@register.simple_tag
def profile_phone(lang=None):
	if lang == 'en':
		content = "Telephone"
	elif lang == 'tr':
		content = "Sabit tel. numarası"
	elif lang == 'bs':
		content = "Telefon"
	elif lang == 'ru':
		content = "Стационарный номер"
	else:
		content = "Telephone"
	return content


@register.simple_tag
def profile_phone_additional(lang=None):
	if lang == 'en':
		content = "Additional number"
	elif lang == 'tr':
		content = "Dahili numarası"
	elif lang == 'bs':
		content = "Dodatni telefon"
	elif lang == 'ru':
		content = "добавочный номер"
	else:
		content = "Additional number"
	return content


@register.simple_tag
def profile_first_name(lang=None):
	if lang == 'en':
		content = "First name"
	elif lang == 'tr':
		content = "İsim"
	elif lang == 'bs':
		content = "Ime"
	elif lang == 'ru':
		content = "Имя"
	else:
		content = "First name"
	return content


@register.simple_tag
def profile_last_name(lang=None):
	if lang == 'en':
		content = "Last name"
	elif lang == 'tr':
		content = "Soyisim"
	elif lang == 'bs':
		content = "Prezime"
	elif lang == 'ru':
		content = "Фамилия"
	else:
		content = "Last name"
	return content


@register.simple_tag
def profile_edit_profile(lang=None):
	if lang == 'en':
		content = "Edit profile"
	elif lang == 'tr':
		content = "Profilimi düzenle"
	elif lang == 'bs':
		content = "Uredi profil"
	elif lang == 'ru':
		content = "Изменить профиль"
	else:
		content = "Edit profile"
	return content


@register.simple_tag
def profile_save_profile(lang=None):
	if lang == 'en':
		content = "Save profile"
	elif lang == 'tr':
		content = "Profilimi kaydet"
	elif lang == 'bs':
		content = "Spremi profil"
	elif lang == 'ru':
		content = "Сохранить профиль"
	else:
		content = "Save profile"
	return content


@register.simple_tag
def general_cancel(lang=None):
	if lang == 'en':
		content = "Cancel"
	elif lang == 'tr':
		content = "İptal"
	elif lang == 'bs':
		content = "Otkaži"
	elif lang == 'ru':
		content = "Отмена"
	else:
		content = "Cancel"
	return content


@register.simple_tag
def general_my_company(lang=None):
	if lang == 'en':
		content = "My company"
	elif lang == 'tr':
		content = "Şirketim"
	elif lang == 'bs':
		content = "Moja firma"
	elif lang == 'ru':
		content = "Моя компания"
	else:
		content = "My company"
	return content


@register.simple_tag
def general_company_branches(lang=None):
	if lang == 'en':
		content = "Company branches"
	elif lang == 'tr':
		content = "Şirket şubeleri"
	elif lang == 'bs':
		content = "Filijale kompanije"
	elif lang == 'ru':
		content = "Филиалы компании"
	else:
		content = "Company branches"
	return content


@register.simple_tag
def general_application_users(lang=None):
	if lang == 'en':
		content = "Application Users"
	elif lang == 'tr':
		content = "Uygulama kullanıcıları"
	elif lang == 'bs':
		content = "Korisnici aplikacije"
	elif lang == 'ru':
		content = "Пользователи приложения"
	else:
		content = "Application Users"
	return content


@register.simple_tag
def general_user_groups(lang=None):
	if lang == 'en':
		content = "User groups"
	elif lang == 'tr':
		content = "Kullanıcı grupları"
	elif lang == 'bs':
		content = "Korisničke grupe"
	elif lang == 'ru':
		content = "Группы пользователей"
	else:
		content = "User groups"
	return content


@register.simple_tag
def general_company_manager(lang=None):
	if lang == 'en':
		content = "Company manager"
	elif lang == 'tr':
		content = "Şirket yönetimi"
	elif lang == 'bs':
		content = "Menadžer kompanije"
	elif lang == 'ru':
		content = "Управление компанийей"
	else:
		content = "Company manager"
	return content


@register.simple_tag
def general_all_rights_reserved(lang=None):
	if lang == 'en':
		content = "All rights reserved"
	elif lang == 'tr':
		content = "Tüm hakları saklıdır"
	elif lang == 'bs':
		content = "Sva prava zadržana"
	elif lang == 'ru':
		content = "Все права защищены"
	else:
		content = "All rights reserved"
	return content


@register.simple_tag
def general_docs(lang=None):
	if lang == 'en':
		content = "Docs"
	elif lang == 'tr':
		content = "Dokumantasyon"
	elif lang == 'bs':
		content = "Dokumentacija"
	elif lang == 'ru':
		content = "Документация"
	else:
		content = "Docs"
	return content


@register.simple_tag
def general_all_products(lang=None):
	if lang == 'en':
		content = "All products"
	elif lang == 'tr':
		content = "Tüm ürünlerimiz"
	elif lang == 'bs':
		content = "Svi naši proizvodi"
	elif lang == 'ru':
		content = "Все продукты"
	else:
		content = "All products"
	return content


@register.simple_tag
def general_manual(lang=None):
	if lang == 'en':
		content = "Manual"
	elif lang == 'tr':
		content = "Kılavuz"
	elif lang == 'bs':
		content = "Priručnik"
	elif lang == 'ru':
		content = "Инструкция"
	else:
		content = "Manual"
	return content


@register.simple_tag
def general_profile_information(lang=None):
	if lang == 'en':
		content = "Profile information"
	elif lang == 'tr':
		content = "Profil bilgileri"
	elif lang == 'bs':
		content = "Informacije o profilu"
	elif lang == 'ru':
		content = "Данные профиля"
	else:
		content = "Profile information"
	return content


@register.simple_tag
def general_change_password(lang=None):
	if lang == 'en':
		content = "Change password"
	elif lang == 'tr':
		content = "Şifre değiştir"
	elif lang == 'bs':
		content = "Promijeni lozinku"
	elif lang == 'ru':
		content = "Изменить пароль"
	else:
		content = "Change password"
	return content


@register.simple_tag
def general_profile_preferences(lang=None):
	if lang == 'en':
		content = "Profile preferences"
	elif lang == 'tr':
		content = "Profil ayarları"
	elif lang == 'bs':
		content = "Postavke profila"
	elif lang == 'ru':
		content = "Настройки профиля"
	else:
		content = "Profile preferences"
	return content


@register.simple_tag
def general_extra_profile_security(lang=None):
	if lang == 'en':
		content = "Extra profile security"
	elif lang == 'tr':
		content = "Ekstra profil güvenliği"
	elif lang == 'bs':
		content = "Dodatna sigurnost profila"
	elif lang == 'ru':
		content = "Доп. защита профиля"
	else:
		content = "Extra profile security"
	return content


@register.simple_tag
def general_client_manager(lang=None):
	if lang == 'en':
		content = "Client manager"
	elif lang == 'tr':
		content = "Müşteri yönetimi"
	elif lang == 'bs':
		content = "Mušterijin upravitelj"
	elif lang == 'ru':
		content = "Управление клиентами"
	else:
		content = "Client manager"
	return content


@register.simple_tag
def general_client_companies(lang=None):
	if lang == 'en':
		content = "Client Companies"
	elif lang == 'tr':
		content = "Müşteri şirketleri"
	elif lang == 'bs':
		content = "Kompanije mušterije"
	elif lang == 'ru':
		content = "Компании клиентов"
	else:
		content = "Client Companies"
	return content


@register.simple_tag
def general_client_branches(lang=None):
	if lang == 'en':
		content = "Client branches"
	elif lang == 'tr':
		content = "Müşteri şubeleri"
	elif lang == 'bs':
		content = "Filijale mušterije"
	elif lang == 'ru':
		content = "Филиалы компаний клиентов"
	else:
		content = "Client branches"
	return content


@register.simple_tag
def general_client_staff(lang=None):
	if lang == 'en':
		content = "Client staff"
	elif lang == 'tr':
		content = "Müsteri personeli"
	elif lang == 'bs':
		content = "Osoblje mušterije"
	elif lang == 'ru':
		content = "Персонал компаний клиентов"
	else:
		content = "Client staff"
	return content


@register.simple_tag
def general_vehicle_agents(lang=None):
	if lang == 'en':
		content = "Vehicle Agents"
	elif lang == 'tr':
		content = "Taşıma ajentaları"
	elif lang == 'bs':
		content = "Zastupnik vozila"
	elif lang == 'ru':
		content = "Агентства грузоперевозок"
	else:
		content = "Vehicle Agents"
	return content


@register.simple_tag
def general_crm_and_trading(lang=None):
	if lang == 'en':
		content = "CRM and Trading"
	elif lang == 'tr':
		content = "CRM ve Pazarlama"
	elif lang == 'bs':
		content = "CRM i prodaja"
	elif lang == 'ru':
		content = "CRM и Трейдинг"
	else:
		content = "CRM and Trading"
	return content


@register.simple_tag
def general_trading_management(lang=None):
	if lang == 'en':
		content = "Trading management"
	elif lang == 'tr':
		content = "Pazarlama yönetimi"
	elif lang == 'bs':
		content = "Upravljanje prodaje"
	elif lang == 'ru':
		content = "Управление торговлей"
	else:
		content = "Trading management"
	return content


@register.simple_tag
def general_offers(lang=None):
	if lang == 'en':
		content = "Offers"
	elif lang == 'tr':
		content = "Teklifler"
	elif lang == 'bs':
		content = "Ponude"
	elif lang == 'ru':
		content = "Предложения"
	else:
		content = "Offers"
	return content


@register.simple_tag
def general_appointments(lang=None):
	if lang == 'en':
		content = "Appointments"
	elif lang == 'tr':
		content = "Görüşmeler"
	elif lang == 'bs':
		content = "Dogovori"
	elif lang == 'ru':
		content = "Собрания и встречи"
	else:
		content = "Appointments"
	return content


@register.simple_tag
def general_reports(lang=None):
	if lang == 'en':
		content = "Reports"
	elif lang == 'tr':
		content = "Raporlar"
	elif lang == 'bs':
		content = "Izvještaji"
	elif lang == 'ru':
		content = "Отчеты"
	else:
		content = "Reports"
	return content


@register.simple_tag
def general_additionals_services(lang=None):
	if lang == 'en':
		content = "Additional services"
	elif lang == 'tr':
		content = "Ek hizmetler"
	elif lang == 'bs':
		content = "Dodatne usluge"
	elif lang == 'ru':
		content = "Дополнительные сервисы"
	else:
		content = "Additional services"
	return content


@register.simple_tag
def general_vehicle_drivers(lang=None):
	if lang == 'en':
		content = "Vehicle drivers"
	elif lang == 'tr':
		content = "Taşıma şoförleri"
	elif lang == 'bs':
		content = "Vozači"
	elif lang == 'ru':
		content = "Водители"
	else:
		content = "Vehicle drivers"
	return content


@register.simple_tag
def general_vehicle_manager(lang=None):
	if lang == 'en':
		content = "Vehicle Manager"
	elif lang == 'tr':
		content = "Taşımacılık yönetimi"
	elif lang == 'bs':
		content = "Upravitelj vozilima"
	elif lang == 'ru':
		content = "Упр. транспортными средствами"
	else:
		content = "Vehicle Manager"
	return content


@register.simple_tag
def general_land_vehicle(lang=None):
	if lang == 'en':
		content = "Land Vehicle"
	elif lang == 'tr':
		content = "Kara taşımacılığı"
	elif lang == 'bs':
		content = "Kopnena vozila"
	elif lang == 'ru':
		content = "Сухопутный транспорт"
	else:
		content = "Land Vehicle"
	return content


@register.simple_tag
def general_air_craft(lang=None):
	if lang == 'en':
		content = "Air Craft"
	elif lang == 'tr':
		content = "Hawa taşımacılığı"
	elif lang == 'bs':
		content = "Avioni"
	elif lang == 'ru':
		content = "Воздушные суда"
	else:
		content = "Air Craft"
	return content


@register.simple_tag
def general_sea_craft(lang=None):
	if lang == 'en':
		content = "Sea Craft"
	elif lang == 'tr':
		content = "Deniz taşımacılığı"
	elif lang == 'bs':
		content = "Brodovi"
	elif lang == 'ru':
		content = "Морские суда"
	else:
		content = "Sea Craft"
	return content


@register.simple_tag
def general_cargo_management(lang=None):
	if lang == 'en':
		content = "Cargo Management"
	elif lang == 'tr':
		content = "Kargo yönetimi"
	elif lang == 'bs':
		content = "Upravljanje dostave"
	elif lang == 'ru':
		content = "Управление грузоперевозками"
	else:
		content = "Cargo Management"
	return content


@register.simple_tag
def general_task_manager(lang=None):
	if lang == 'en':
		content = "Task Manager"
	elif lang == 'tr':
		content = "Görev yöneticisi"
	elif lang == 'bs':
		content = "Upravitelj zadataka"
	elif lang == 'ru':
		content = "Управление задачами"
	else:
		content = "Task Manager"
	return content


@register.simple_tag
def general_messages_and_chats(lang=None):
	if lang == 'en':
		content = "Messages and Chats"
	elif lang == 'tr':
		content = "Mesajlar ve sohbetler"
	elif lang == 'bs':
		content = "Poruke i chat"
	elif lang == 'ru':
		content = "Сообщения и чаты"
	else:
		content = "Messages and Chats"
	return content


@register.simple_tag
def general_faq(lang=None):
	if lang == 'en':
		content = "F.A.Q"
	elif lang == 'tr':
		content = "S.S.S."
	elif lang == 'bs':
		content = "F.A.Q"
	elif lang == 'ru':
		content = "Ч.З.В."
	else:
		content = "F.A.Q"
	return content


@register.simple_tag
def general_tutorials(lang=None):
	if lang == 'en':
		content = "Tutorials"
	elif lang == 'tr':
		content = "Öğretici içerikler"
	elif lang == 'bs':
		content = "Tutorijali"
	elif lang == 'ru':
		content = "Обучающие уроки"
	else:
		content = "Tutorials"
	return content


@register.simple_tag
def general_commercial_offers(lang=None):
	if lang == 'en':
		content = "Commercial offers"
	elif lang == 'tr':
		content = "İş teklifleri"
	elif lang == 'bs':
		content = "Poslovne ponude"
	elif lang == 'ru':
		content = "Коммерческие предложения"
	else:
		content = "Commercial offers"
	return content


@register.simple_tag
def general_to_filter(lang=None):
	if lang == 'en':
		content = "Filter"
	elif lang == 'tr':
		content = "Filtrele"
	elif lang == 'bs':
		content = "Filter"
	elif lang == 'ru':
		content = "Фильтровать"
	else:
		content = "Filter"
	return content


@register.simple_tag
def general_commercial_offer(lang=None):
	if lang == 'en':
		content = "Commercial offer"
	elif lang == 'tr':
		content = "İş teklifi"
	elif lang == 'bs':
		content = "Poslovna ponuda"
	elif lang == 'ru':
		content = "Коммерческое предложение"
	else:
		content = "Commercial offer"
	return content


@register.simple_tag
def general_filter(lang=None):
	if lang == 'en':
		content = "Filter"
	elif lang == 'tr':
		content = "Filtrele"
	elif lang == 'bs':
		content = "Filter"
	elif lang == 'ru':
		content = "Фильтр"
	else:
		content = "Filter"
	return content


@register.simple_tag
def general_order_by(lang=None):
	if lang == 'en':
		content = "Order by"
	elif lang == 'tr':
		content = "Sırala"
	elif lang == 'bs':
		content = "Poredaj"
	elif lang == 'ru':
		content = "Сортировать по"
	else:
		content = "Order by"
	return content


@register.simple_tag
def general_show_by(lang=None):
	if lang == 'en':
		content = "Show by"
	elif lang == 'tr':
		content = "Göster"
	elif lang == 'bs':
		content = "Prikaži po"
	elif lang == 'ru':
		content = "Показывать по"
	else:
		content = "Show by"
	return content


@register.simple_tag
def general_first(lang=None):
	if lang == 'en':
		content = "First"
	elif lang == 'tr':
		content = "Grad"
	elif lang == 'bs':
		content = "Prvi"
	elif lang == 'ru':
		content = "Первый"
	else:
		content = "First"
	return content


@register.simple_tag
def general_last(lang=None):
	if lang == 'en':
		content = "Last"
	elif lang == 'tr':
		content = "Son"
	elif lang == 'bs':
		content = "Zadnji"
	elif lang == 'ru':
		content = "Последний"
	else:
		content = "Last"
	return content


@register.simple_tag
def general_next(lang=None):
	if lang == 'en':
		content = "Next"
	elif lang == 'tr':
		content = "Sonraki"
	elif lang == 'bs':
		content = "Sljedeći"
	elif lang == 'ru':
		content = "Следующий"
	else:
		content = "Next"
	return content


@register.simple_tag
def general_previous(lang=None):
	if lang == 'en':
		content = "Previous"
	elif lang == 'tr':
		content = "Önceki"
	elif lang == 'bs':
		content = "Prethodni"
	elif lang == 'ru':
		content = "Предидущий"
	else:
		content = "Previous"
	return content


@register.simple_tag
def general_save(lang=None):
	if lang == 'en':
		content = "Save"
	elif lang == 'tr':
		content = "Kaydet"
	elif lang == 'bs':
		content = "Spremi"
	elif lang == 'ru':
		content = "Сохранить"
	else:
		content = "Save"
	return content


@register.simple_tag
def general_edit(lang=None):
	if lang == 'en':
		content = "Edit"
	elif lang == 'tr':
		content = "Değiştir"
	elif lang == 'bs':
		content = "Uredi"
	elif lang == 'ru':
		content = "Изменить"
	else:
		content = "Edit"
	return content


@register.simple_tag
def general_yes(lang=None):
	if lang == 'en':
		content = "Yes"
	elif lang == 'tr':
		content = "Evet"
	elif lang == 'bs':
		content = "Da"
	elif lang == 'ru':
		content = "Да"
	else:
		content = "Yes"
	return content


@register.simple_tag
def general_no(lang=None):
	if lang == 'en':
		content = "No"
	elif lang == 'tr':
		content = "Hayır"
	elif lang == 'bs':
		content = "Ne"
	elif lang == 'ru':
		content = "Нет"
	else:
		content = "No"
	return content


@register.simple_tag
def general_branch(lang=None):
	if lang == 'en':
		content = "Branch"
	elif lang == 'tr':
		content = "Şube"
	elif lang == 'bs':
		content = "Filijala"
	elif lang == 'ru':
		content = "Филиал"
	else:
		content = "Branch"
	return content


@register.simple_tag
def general_branch_name(lang=None):
	if lang == 'en':
		content = "Branch name"
	elif lang == 'tr':
		content = "Şube adı"
	elif lang == 'bs':
		content = "Ime filijale"
	elif lang == 'ru':
		content = "Название филиала"
	else:
		content = "Branch name"
	return content


@register.simple_tag
def general_company_name(lang=None):
	if lang == 'en':
		content = "Company name"
	elif lang == 'tr':
		content = "Şirket ünvanı"
	elif lang == 'bs':
		content = "Ime kompanije"
	elif lang == 'ru':
		content = "Название компание"
	else:
		content = "Company name"
	return content


@register.simple_tag
def general_save_changes(lang=None):
	if lang == 'en':
		content = "Save changes"
	elif lang == 'tr':
		content = "Değişiklikleri kaydet"
	elif lang == 'bs':
		content = "Spremi promjene"
	elif lang == 'ru':
		content = "Сохранить изменения"
	else:
		content = "Save changes"
	return content


@register.simple_tag
def general_main_office(lang=None):
	if lang == 'en':
		content = "Main office"
	elif lang == 'tr':
		content = "Merkez ofis"
	elif lang == 'bs':
		content = "Glavni ured"
	elif lang == 'ru':
		content = "Главный офис"
	else:
		content = "Main office"
	return content


@register.simple_tag
def general_fax_number(lang=None):
	if lang == 'en':
		content = "Fax number"
	elif lang == 'tr':
		content = "Faks numarası"
	elif lang == 'bs':
		content = "Faks"
	elif lang == 'ru':
		content = "Номер факса"
	else:
		content = "Fax number"
	return content


@register.simple_tag
def general_logo(lang=None):
	if lang == 'en':
		content = "Logo"
	elif lang == 'tr':
		content = "Logo"
	elif lang == 'bs':
		content = "Logo"
	elif lang == 'ru':
		content = "Логотип"
	else:
		content = "Logo"
	return content


@register.simple_tag
def general_avatar(lang=None):
	if lang == 'en':
		content = "Avatar"
	elif lang == 'tr':
		content = "Avatar"
	elif lang == 'bs':
		content = "Avatar"
	elif lang == 'ru':
		content = "Аватар"
	else:
		content = "Avatar"
	return content


@register.simple_tag
def general_profile_image(lang=None):
	if lang == 'en':
		content = "Profile image"
	elif lang == 'tr':
		content = "Profil resmi"
	elif lang == 'bs':
		content = "Slika profila"
	elif lang == 'ru':
		content = "Фотография профиля"
	else:
		content = "Profile image"
	return content


@register.simple_tag
def general_crop(lang=None):
	if lang == 'en':
		content = "Crop"
	elif lang == 'tr':
		content = "Kes"
	elif lang == 'bs':
		content = "Isijeci"
	elif lang == 'ru':
		content = "Обрезать"
	else:
		content = "Crop"
	return content


@register.simple_tag
def general_support_information(lang=None):
	if lang == 'en':
		content = "Support information"
	elif lang == 'tr':
		content = "Destek bilgileri"
	elif lang == 'bs':
		content = "Informacija o podršci"
	elif lang == 'ru':
		content = "Справочная информация"
	else:
		content = "Support information"
	return content


@register.simple_tag
def company_staff_manager(lang=None):
	if lang == 'en':
		content = "Staff Manager"
	elif lang == 'tr':
		content = "Personel yönetimi"
	elif lang == 'bs':
		content = "Upravitelj zaposlenicima"
	elif lang == 'ru':
		content = "Управление персоналом"
	else:
		content = "Staff Manager"
	return content


@register.simple_tag
def company_employees(lang=None):
	if lang == 'en':
		content = "Employees"
	elif lang == 'tr':
		content = "Elemanlar"
	elif lang == 'bs':
		content = "Zaposlenici"
	elif lang == 'ru':
		content = "Работники"
	else:
		content = "Employees"
	return content


@register.simple_tag
def company_positions(lang=None):
	if lang == 'en':
		content = "Positions"
	elif lang == 'tr':
		content = "Pozisyonlar"
	elif lang == 'bs':
		content = "Pozicije"
	elif lang == 'ru':
		content = "Должности"
	else:
		content = "Positions"
	return content


@register.simple_tag
def company_position(lang=None):
	if lang == 'en':
		content = "Position"
	elif lang == 'tr':
		content = "Pozisyon"
	elif lang == 'bs':
		content = "Pozicija"
	elif lang == 'ru':
		content = "Должность"
	else:
		content = "Position"
	return content


@register.simple_tag
def company_company_position(lang=None):
	if lang == 'en':
		content = "Company position"
	elif lang == 'tr':
		content = "İş pozisyonu"
	elif lang == 'bs':
		content = "Pozicija u kompaniji"
	elif lang == 'ru':
		content = "Должность в компании"
	else:
		content = "Company position"
	return content


@register.simple_tag
def company_company_positions(lang=None):
	if lang == 'en':
		content = "Company positions"
	elif lang == 'tr':
		content = "İş pozisyonları"
	elif lang == 'bs':
		content = "Pozicije u kompanije"
	elif lang == 'ru':
		content = "Должности в компании"
	else:
		content = "Company positions"
	return content


@register.simple_tag
def company_employee(lang=None):
	if lang == 'en':
		content = "Employee"
	elif lang == 'tr':
		content = "Eleman"
	elif lang == 'bs':
		content = "Zaposlenik"
	elif lang == 'ru':
		content = "Работник"
	else:
		content = "Employee"
	return content


@register.simple_tag
def company_staff_list(lang=None):
	if lang == 'en':
		content = "Staff List"
	elif lang == 'tr':
		content = "Personel listesi"
	elif lang == 'bs':
		content = "Lista zaposlenika"
	elif lang == 'ru':
		content = "Список персонала"
	else:
		content = "Staff List"
	return content


@register.simple_tag
def general_add(lang=None):
	if lang == 'en':
		content = "Add"
	elif lang == 'tr':
		content = "Ekle"
	elif lang == 'bs':
		content = "Dodati"
	elif lang == 'ru':
		content = "Добавить"
	else:
		content = "Add"
	return content


@register.simple_tag
def general_delete(lang=None):
	if lang == 'en':
		content = "Delete"
	elif lang == 'tr':
		content = "Sil"
	elif lang == 'bs':
		content = "Izbrisati"
	elif lang == 'ru':
		content = "Удалить"
	else:
		content = "Delete"
	return content


@register.simple_tag
def general_enable(lang=None):
	if lang == 'en':
		content = "Enable"
	elif lang == 'tr':
		content = "Etkinleştir"
	elif lang == 'bs':
		content = "Omogući"
	elif lang == 'ru':
		content = "Включить"
	else:
		content = "Enable"
	return content


@register.simple_tag
def general_disable(lang=None):
	if lang == 'en':
		content = "Disable"
	elif lang == 'tr':
		content = "Devre dışı"
	elif lang == 'bs':
		content = "Onemogući"
	elif lang == 'ru':
		content = "Отключить"
	else:
		content = "Disable"
	return content


@register.simple_tag
def general_activate(lang=None):
	if lang == 'en':
		content = "Activate"
	elif lang == 'tr':
		content = "Aktif et"
	elif lang == 'bs':
		content = "Aktiviraj"
	elif lang == 'ru':
		content = "Активировать"
	else:
		content = "Activate"
	return content


@register.simple_tag
def general_deactivate(lang=None):
	if lang == 'en':
		content = "Deactivate"
	elif lang == 'tr':
		content = "Devre dışı bırak"
	elif lang == 'bs':
		content = "Deaktiviraj"
	elif lang == 'ru':
		content = "Деактивировать"
	else:
		content = "Deactivate"
	return content


@register.simple_tag
def general_block(lang=None):
	if lang == 'en':
		content = "Block"
	elif lang == 'tr':
		content = "Bloke et"
	elif lang == 'bs':
		content = "Blokiraj"
	elif lang == 'ru':
		content = "Заблокировать"
	else:
		content = "Block"
	return content


@register.simple_tag
def general_unblock(lang=None):
	if lang == 'en':
		content = "Unblock"
	elif lang == 'tr':
		content = "Blokeyi kaldır"
	elif lang == 'bs':
		content = "Deblokiraj"
	elif lang == 'ru':
		content = "Разблокировать"
	else:
		content = "Unblock"
	return content


