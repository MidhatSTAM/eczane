# -*- coding:utf-8 -*-
from __future__ import unicode_literals

import datetime

from django import template
register = template.Library()


@register.simple_tag
def left_days(start, end):
	date_format = '%Y-%m-%d %H:%M:%S'
	start_date = datetime.datetime.strptime(
		str(start)[:19], date_format
	)
	end_date = datetime.datetime.strptime(
		str(end)[:19], date_format
	)
	today_date = datetime.datetime.strptime(
		str(datetime.datetime.now())[:19], date_format
	)
	
	start_end_range = (end_date - start_date).days
	elapsed_range = (end_date - today_date).days
	return float("{0:.2f}".format(float(elapsed_range) / float(start_end_range) * 100))
