# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from .models import Pharmacies, Productbrands, Products, Cities, Counties, Districts, Persons, Addresses, Contacttypes, \
	Contactinfos, PersonsHasContactinfos, Buyingproducts, Productsales, Appuserlicenses


class PharmacyForm(forms.ModelForm):
	class Meta:
		model = Pharmacies
		exclude = (
			'name',
			'description',
		)


class ProductbrandsForm(forms.ModelForm):
	class Meta:
		model = Productbrands
		exclude = (
			'name',
		)


class ProductForm(forms.ModelForm):
	class Meta:
		model = Products
		exclude = (
			'barcode',
			'name',
			'description',
			'active',
			'productbrands',
		)


class CityForm(forms.ModelForm):
	class Meta:
		model = Cities
		exclude = (
			'city',
		)


class CountyForm(forms.ModelForm):
	class Meta:
		model = Counties
		exclude = (
			'county',
			'cities',
		)


class DistrictForm(forms.ModelForm):
	class Meta:
		model = Districts
		exclude = (
			'district',
			'counties',
			'counties_cities',
		)


class PersonForm(forms.ModelForm):
	class Meta:
		model = Persons
		exclude = (
			'name',
			'surname',
			'username',
			'gender',
			'password',
			'admin',
			'active',
			'cdate',
			'udate',
		)


class AddressForm(forms.ModelForm):
	class Meta:
		model = Addresses
		exclude = (
			'description',
			'otheraddrinfo',
			'active',
			'cities',
			'counties',
			'districts',
		)


class ContacttypesForm(forms.ModelForm):
	class Meta:
		model = Contacttypes
		exclude = (
			'name',
		)


class ContactForm(forms.ModelForm):
	class Meta:
		model = Contactinfos
		exclude = (
			'text',
			'active',
			'cdate',
			'udate',
			'contacttypes',
		)


class PersonhascontactForm(forms.ModelForm):
	class Meta:
		model = PersonsHasContactinfos
		exclude = (
			'persons',
			'contactinfos',
		)


class ProductBuyForm(forms.ModelForm):
	class Meta:
		model = Buyingproducts
		exclude = (
			'buydate',
			'description',
			'quantity',
			'cdate',
			'udate',
			'products',
		)


class ProductSaleForm(forms.ModelForm):
	class Meta:
		model = Productsales
		exclude = (
			'saledate',
			'description',
			'quantity',
			'cdate',
			'udate',
			'products',
			'pharmacies',
		)
		
		
class UserLicenseForm(forms.ModelForm):
	class Meta:
		model = Appuserlicenses
		exclude = (
			'active',
			'startingdate',
			'enddate',
			'data',
			'cdate',
			'udate',
			'pharmacies',
			'persons',
		)


class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
