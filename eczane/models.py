# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

import sys
reload(sys)
sys.setdefaultencoding('utf8')


class Addresses(models.Model):
    class Meta:
        managed = False
        db_table = 'ADDRESSES'
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=45,
        blank=True,
        null=True
    )  # Field name made lowercase.

    otheraddrinfo = models.CharField(
        db_column='OTHERADDRINFO',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    releated_id = models.IntegerField(
        db_column='RELEATED_ID',
        blank=True,
        null=True
    )  # Field name made lowercase.

    districts = models.ForeignKey(
        'Districts',
        models.DO_NOTHING,
        db_column='DISTRICTS_ID',
        related_name="addresses_districts_key"
    )  # Field name made lowercase.

    counties = models.ForeignKey(
        'Districts',
        models.DO_NOTHING,
        db_column='COUNTIES_ID',
        related_name="addresses_counties_key"
    )  # Field name made lowercase.

    cities = models.ForeignKey(
        'Districts',
        models.DO_NOTHING,
        db_column='CITIES_ID',
        related_name="addresses_cities_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.description.lower()


class Advbanners(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVBANNERS'
        verbose_name = "Advbanner"
        verbose_name_plural = "Advbanners"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.description.lower()


class AdvbannersHasAdvcampaigns(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVBANNERS_has_ADVCAMPAIGNS'
        unique_together = (
            'advcampaigns',
        )
        verbose_name = "AdvbannersHasAdvcampaign"
        verbose_name_plural = "AdvbannersHasAdvcampaigns"

    advbanners = models.OneToOneField(
        Advbanners,
        models.DO_NOTHING,
        db_column='ADVBANNERS_ID',
        primary_key=True,
        related_name="advbannershasadvcampaigns_advbanners_key"
    )  # Field name made lowercase.

    advcampaigns = models.ForeignKey(
        'Advcampaigns',
        models.DO_NOTHING,
        db_column='ADVCAMPAIGNS_ID',
        related_name="advbannershasadvcampaigns_advcampaigns_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.advbanners.description) + " - " + str(self.advcampaigns.description))


class AdvbannersHasAdvmediafiles(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVBANNERS_has_ADVMEDIAFILES'
        unique_together = (
            'advmediafiles',
        )
        verbose_name = "AdvbannersHasAdvmediafile"
        verbose_name_plural = "AdvbannersHasAdvmediafiles"

    advbanners = models.OneToOneField(
        Advbanners,
        models.DO_NOTHING,
        db_column='ADVBANNERS_ID',
        primary_key=True,
        related_name="advbannershasadvmediafiles_advbanners_key"
    )  # Field name made lowercase.

    advmediafiles = models.ForeignKey(
        'Advmediafiles',
        models.DO_NOTHING,
        db_column='ADVMEDIAFILES_ID',
        related_name="advbannershasadvmediafiles_advmediafiles_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.advbanners.description) + " - " + str(self.advmediafiles.description))


class Advcampaigns(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVCAMPAIGNS'
        verbose_name = "Advcampaign"
        verbose_name_plural = "Advcampaigns"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=100
    )  # Field name made lowercase.

    startingdate = models.DateTimeField(
        db_column='STARTINGDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    enddate = models.DateTimeField(
        db_column='ENDDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE'
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE'
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.description.lower()


class AdvcampaignsHasAdvregions(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVCAMPAIGNS_has_ADVREGIONS'
        unique_together = (
            'advregions',
        )
        verbose_name = "AdvcampaignsHasAdvregion"
        verbose_name_plural = "AdvcampaignsHasAdvregions"

    advcampaigns = models.OneToOneField(
        Advcampaigns,
        models.DO_NOTHING,
        db_column='ADVCAMPAIGNS_ID',
        primary_key=True,
        related_name="advcampaignshasadvregions_advcampaigns_key"
    )  # Field name made lowercase.

    advregions = models.ForeignKey(
        'Advregions',
        models.DO_NOTHING,
        db_column='ADVREGIONS_ID',
        related_name="advcampaignshasadvregions_advregions_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.advcampaigns.description) + " - AdvRegion " + str(self.advregions.id))


class Advmediafiles(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVMEDIAFILES'
        verbose_name = "Advmediafile"
        verbose_name_plural = "Advmediafiles"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    ofilename = models.CharField(
        db_column='OFILENAME',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    md5file = models.CharField(
        db_column='MD5FILE',
        max_length=32,
        blank=True,
        null=True
    )  # Field name made lowercase.

    filetype = models.CharField(
        db_column='FILETYPE',
        max_length=3,
        blank=True,
        null=True
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.description.lower()


class Advregions(models.Model):
    class Meta:
        managed = False
        db_table = 'ADVREGIONS'
        verbose_name = "Advregion"
        verbose_name_plural = "Advregions"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    startingdate = models.DateTimeField(
        db_column='STARTINGDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    enddate = models.DateTimeField(
        db_column='ENDDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE'
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE'
    )  # Field name made lowercase.

    def __unicode__(self):
        return "AdvRegion " + str(self.id)


class Appuserlicenses(models.Model):
    class Meta:
        managed = False
        db_table = 'APPUSERLICENSES'
        unique_together = (
            ('id', 'pharmacies', 'persons'),
        )
        verbose_name = "Appuserlicense"
        verbose_name_plural = "Appuserlicenses"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE'
    )  # Field name made lowercase.

    startingdate = models.DateTimeField(
        db_column='STARTINGDATE'
    )  # Field name made lowercase.

    enddate = models.DateTimeField(
        db_column='ENDDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    data = models.CharField(
        db_column='DATA',
        max_length=255,
        blank=True,
        null=True
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    pharmacies = models.ForeignKey(
        'Pharmacies',
        models.DO_NOTHING,
        db_column='PHARMACIES_ID',
        related_name="appuserlicenses_pharmacies_key"
    )  # Field name made lowercase.

    persons = models.ForeignKey(
        'Persons',
        models.DO_NOTHING,
        db_column='PERSONS_ID',
        related_name="appuserslicenses_persons_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(self.persons.username) + " license"


class Buyingproducts(models.Model):
    class Meta:
        managed = False
        db_table = 'BUYINGPRODUCTS'
        unique_together = (
            ('id', 'products'),
        )
        verbose_name = "Buyingproduct"
        verbose_name_plural = "Buyingproducts"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    buydate = models.DateTimeField(
        db_column='BUYDATE',
        blank=True,
        null=True)
    # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    quantity = models.IntegerField(
        db_column='QUANTITY'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    products = models.ForeignKey(
        'Products',
        models.DO_NOTHING,
        db_column='PRODUCTS_ID',
        related_name="buyingproducts_products_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.description.lower()


class Cities(models.Model):
    class Meta:
        managed = False
        db_table = 'CITIES'
        verbose_name = "City"
        verbose_name_plural = "Cities"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    city = models.CharField(
        db_column='CITY',
        unique=True,
        max_length=30
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.city


class Contactinfos(models.Model):
    class Meta:
        managed = False
        db_table = 'CONTACTINFOS'
        unique_together = (
            ('id', 'contacttypes'),
        )
        verbose_name = "Contactinfo"
        verbose_name_plural = "Contactinfos"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    text = models.CharField(
        db_column='TEXT',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE'
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE'
    )  # Field name made lowercase.

    contacttypes = models.ForeignKey(
        'Contacttypes',
        models.DO_NOTHING,
        db_column='CONTACTTYPES_ID',
        related_name="contactinfos_contacttypes_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.text


class Contacttypes(models.Model):
    class Meta:
        managed = False
        db_table = 'CONTACTTYPES'
        verbose_name = "Contacttype"
        verbose_name_plural = "Contacttypes"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    name = models.CharField(
        db_column='NAME',
        max_length=15
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.name


class Counties(models.Model):
    class Meta:
        managed = False
        db_table = 'COUNTIES'
        unique_together = (
            ('id', 'cities'),
        )
        verbose_name = "County"
        verbose_name_plural = "Counties"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    county = models.CharField(
        db_column='COUNTY',
        max_length=30
    )  # Field name made lowercase.

    cities = models.ForeignKey(
        Cities,
        models.DO_NOTHING,
        db_column='CITIES_ID',
        related_name="counties_cities_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.county


class Districts(models.Model):
    class Meta:
        managed = False
        db_table = 'DISTRICTS'
        unique_together = (
            ('id', 'counties', 'counties_cities'),
        )
        verbose_name = "District"
        verbose_name_plural = "Districts"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    district = models.CharField(
        db_column='DISTRICT',
        max_length=30
    )  # Field name made lowercase.

    counties = models.ForeignKey(
        Counties,
        models.DO_NOTHING,
        db_column='COUNTIES_ID',
        related_name="districts_counties_key"
    )  # Field name made lowercase.

    counties_cities = models.ForeignKey(
        Counties,
        models.DO_NOTHING,
        db_column='COUNTIES_CITIES_ID',
        related_name="districts_counties_cities_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.district


class Persons(models.Model):
    class Meta:
        managed = False
        db_table = 'PERSONS'
        verbose_name = "Person"
        verbose_name_plural = "Persons"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    name = models.CharField(
        db_column='NAME',
        max_length=25
    )  # Field name made lowercase.

    surname = models.CharField(
        db_column='SURNAME',
        max_length=25
    )  # Field name made lowercase.

    username = models.CharField(
        db_column='USERNAME',
        unique=True,
        max_length=20
    )  # Field name made lowercase.

    gender = models.CharField(
        db_column='GENDER',
        max_length=1
    )  # Field name made lowercase.

    password = models.CharField(
        db_column='PASSWORD',
        max_length=32
    )  # Field name made lowercase.

    admin = models.IntegerField(
        db_column='ADMIN'
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE'
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE'
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.username.lower()


class PersonsHasAddresses(models.Model):
    class Meta:
        managed = False
        db_table = 'PERSONS_has_ADDRESSES'
        verbose_name = "PersonsHasAddress"
        verbose_name_plural = "PersonsHasAddresses"

    persons = models.OneToOneField(
        Persons,
        models.DO_NOTHING,
        db_column='PERSONS_ID',
        primary_key=True,
        related_name="personshasaddresses_persons_key"
    )  # Field name made lowercase.

    addresses = models.OneToOneField(
        Addresses,
        models.DO_NOTHING,
        db_column='ADDRESSES_ID',
        unique=True,
        related_name="personshasaddresses_addresses_key"
    )  # Field name made lowercase.


class PersonsHasContactinfos(models.Model):
    class Meta:
        managed = False
        db_table = 'PERSONS_has_CONTACTINFOS'
        verbose_name = "PersonsHasContactinfo"
        verbose_name_plural = "PersonsHasContactinfos"

    persons = models.OneToOneField(
        Persons,
        models.DO_NOTHING,
        db_column='PERSONS_ID',
        primary_key=True,
        related_name="personshascontactinfos_persons_key"
    )  # Field name made lowercase.

    contactinfos = models.OneToOneField(
        Contactinfos,
        models.DO_NOTHING,
        db_column='CONTACTINFOS_ID',
        unique=True,
        related_name="personshascontactinfos_contactinfos"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.persons.username) + "'s contact informations")


class Pharmacies(models.Model):
    class Meta:
        managed = False
        db_table = 'PHARMACIES'
        verbose_name = "Pharmacy"
        verbose_name_plural = "Pharmacies"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    name = models.CharField(
        db_column='NAME',
        unique=True,
        max_length=60
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=255,
        blank=True,
        null=True
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.name


class PharmaciesHasAddresses(models.Model):
    class Meta:
        managed = False
        db_table = 'PHARMACIES_has_ADDRESSES'
        verbose_name = "PharmaciesHasAddress"
        verbose_name_plural = "PharmaciesHasAddresses"

    pharmacies = models.OneToOneField(
        Pharmacies,
        models.DO_NOTHING,
        db_column='PHARMACIES_ID',
        primary_key=True,
        related_name="pharmacieshasaddresses_pharmacies_key"
    )  # Field name made lowercase.

    addresses = models.OneToOneField(
        Addresses,
        models.DO_NOTHING,
        db_column='ADDRESSES_ID',
        unique=True,
        related_name="pharmacieshasaddresses_addresses_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.pharmacies.name) + "'s address")


class PharmaciesHasContactinfos(models.Model):
    class Meta:
        managed = False
        db_table = 'PHARMACIES_has_CONTACTINFOS'
        verbose_name = "PharmaciesHasContactinfo"
        verbose_name_plural = "PharmaciesHasContactinfos"

    pharmacies = models.OneToOneField(
        Pharmacies,
        models.DO_NOTHING,
        db_column='PHARMACIES_ID',
        primary_key=True,
        related_name="pharmacieshascontactinfos_pharmacies_key"
    )  # Field name made lowercase.

    contactinfos = models.OneToOneField(
        Contactinfos,
        models.DO_NOTHING,
        db_column='CONTACTINFOS_ID',
        unique=True,
        related_name="pharmacieshascontactinfos_contactinfos_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.pharmacies.name) + "'s contact informations")


class Productbrands(models.Model):
    class Meta:
        managed = False
        db_table = 'PRODUCTBRANDS'
        verbose_name = "Productbrand"
        verbose_name_plural = "Productbrands"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    name = models.CharField(
        db_column='NAME',
        unique=True,
        max_length=45
    )  # Field name made lowercase.

    def __unicode__(self):
        return self.name


class Products(models.Model):
    class Meta:
        managed = False
        db_table = 'PRODUCTS'
        unique_together = (
            ('id', 'productbrands'),
        )
        verbose_name = "Product"
        verbose_name_plural = "Products"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    barcode = models.CharField(
        db_column='BARCODE',
        unique=True,
        max_length=20
    )  # Field name made lowercase.

    name = models.CharField(
        db_column='NAME',
        max_length=100
    )  # Field name made lowercase.

    description = models.TextField(
        db_column='DESCRIPTION',
        blank=True,
        null=True
    )  # Field name made lowercase.

    active = models.IntegerField(
        db_column='ACTIVE'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE',
        blank=True,
        null=True
    )  # Field name made lowercase.

    productbrands = models.ForeignKey(
        Productbrands,
        models.DO_NOTHING,
        db_column='PRODUCTBRANDS_ID',
        related_name="products_productbrands_key"
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.name) + " by " + str(self.productbrands.name))


class Productsales(models.Model):
    class Meta:
        managed = False
        db_table = 'PRODUCTSALES'
        unique_together = (
            ('id', 'products', 'pharmacies'),
        )
        verbose_name = "Productsale"
        verbose_name_plural = "Productsales"

    id = models.AutoField(
        db_column='ID',
        primary_key=True
    )  # Field name made lowercase.

    products = models.ForeignKey(
        Products,
        models.DO_NOTHING,
        db_column='PRODUCTS_ID',
        related_name="productsales_products_key"
    )  # Field name made lowercase.

    pharmacies = models.ForeignKey(
        Pharmacies,
        models.DO_NOTHING,
        db_column='PHARMACIES_ID',
        related_name="productsales_pharmacies_key"
    )  # Field name made lowercase.

    saledate = models.DateTimeField(
        db_column='SALEDATE'
    )  # Field name made lowercase.

    description = models.CharField(
        db_column='DESCRIPTION',
        max_length=100,
        blank=True,
        null=True
    )  # Field name made lowercase.

    quantity = models.IntegerField(
        db_column='QUANTITY'
    )  # Field name made lowercase.

    cdate = models.DateTimeField(
        db_column='CDATE'
    )  # Field name made lowercase.

    udate = models.DateTimeField(
        db_column='UDATE'
    )  # Field name made lowercase.

    def __unicode__(self):
        return str(str(self.products.name) + "'s sales")


class DjangoMigrations(models.Model):
    class Meta:
        managed = False
        db_table = 'django_migrations'

    app = models.CharField(
        max_length=255
    )

    name = models.CharField(
        max_length=255
    )

    applied = models.DateTimeField()
