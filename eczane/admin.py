# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from eczane.models import *
# Register your models here.


class CountiesAdmin(admin.ModelAdmin):
	list_display = (
		'cities',
		'county',
	)
	list_filter = [
		'cities'
	]

admin.site.register(Persons)
admin.site.register(Addresses)
admin.site.register(Advbanners)
admin.site.register(AdvbannersHasAdvcampaigns)
admin.site.register(AdvbannersHasAdvmediafiles)
admin.site.register(Advcampaigns)
admin.site.register(AdvcampaignsHasAdvregions)
admin.site.register(Advmediafiles)
admin.site.register(Advregions)
admin.site.register(Appuserlicenses)
admin.site.register(Buyingproducts)
admin.site.register(Cities)
admin.site.register(Contactinfos)
admin.site.register(Contacttypes)
admin.site.register(Counties, CountiesAdmin)
admin.site.register(Districts)
admin.site.register(PersonsHasAddresses)
admin.site.register(PersonsHasContactinfos)
admin.site.register(Pharmacies)
admin.site.register(PharmaciesHasAddresses)
admin.site.register(PharmaciesHasContactinfos)
admin.site.register(Productbrands)
admin.site.register(Products)
admin.site.register(Productsales)