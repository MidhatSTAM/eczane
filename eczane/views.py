# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import hashlib

from django.core.serializers import json

reload(sys)
sys.setdefaultencoding('utf8')

import django.contrib.auth
from django.shortcuts import render
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core.exceptions import ObjectDoesNotExist

from itertools import chain

from django.utils.dateformat import DateFormat
import datetime
from django.utils import formats

from django.db.models import Count
from django.db.models import FloatField, IntegerField, DateTimeField, DateField
from django.db.models.functions import Cast, Coalesce

from .decorators import user_login_required
from .models import Pharmacies, Productbrands, Products, Cities, Counties, Districts, Persons, Addresses, Contacttypes, \
	PersonsHasAddresses, PersonsHasContactinfos, Contactinfos, Buyingproducts, Productsales, Appuserlicenses, \
	PharmaciesHasAddresses, PharmaciesHasContactinfos
from .forms import PharmacyForm, ProductbrandsForm, ProductForm, CityForm, CountyForm, DistrictForm, PersonForm, \
	AddressForm, ContacttypesForm, ContactForm, PersonhascontactForm, ProductBuyForm, ProductSaleForm, UserLicenseForm, \
	LoginForm

# Create your views here.

args = {}


def user_login(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = django.contrib.auth.authenticate(username=username, password=password)
	url_redirect = request.GET.get('next', '')
	if len(url_redirect) >= 1:
		url_redirect = request.GET.get('next')
	else:
		url_redirect = "/"
	
	if user is not None:
		#		return HttpResponseRedirect(url_redirect)
		# the password verified for the user
		if user.is_active:
			django.contrib.auth.login(request, user)
			return HttpResponseRedirect(url_redirect)
		else:
			return HttpResponseRedirect("/login/")
	else:
		return HttpResponseRedirect("/login/")


@user_login_required
def homepage(request):
	args['last_pharmacies'] = Pharmacies.objects.all().order_by('-id')[:50]
	args['last_product_buy'] = Buyingproducts.objects.all().order_by('-id')[:50]
	args['last_product_sale'] = Productsales.objects.all().order_by('-id')[:50]
	args['expiring_user_licenses'] = Appuserlicenses.objects.all().order_by('enddate')[:50]
	args['last_products'] = Products.objects.all().order_by('-id')[:50]
	
	args['all_buy_dates'] = Buyingproducts.objects.values('buydate').order_by('buydate').annotate(count=Count('buydate'))
	args['all_sale_dates'] = Productsales.objects.values('saledate').order_by('saledate').annotate(count=Count('saledate'))
	
	args['buy_counts'] = Buyingproducts.objects.values('buydate').order_by('buydate').annotate(count=Count('buydate'))
	args['sale_counts'] = Productsales.objects.values('saledate').order_by('saledate').annotate(count=Count('saledate'))
	
	args['pharmacies_has_address'] = PharmaciesHasAddresses.objects.all()
	args['user_count'] = total_users = Persons.objects.count()
	args['pharmacy_count'] = pharmacy_count = Pharmacies.objects.count()
	args['product_count'] = product_count = Products.objects.count()
	args['product_brands_count'] = product_brands_count = Productbrands.objects.count()
	return render(request, 'home.html', args)


@user_login_required
def pharmacy_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_pharmacy_by', ''))) >= 1:
		show_pharmacy_by = request.COOKIES.get('show_pharmacy_by')
	else:
		show_pharmacy_by = 20
	
	args['pharmacy'] = pharmacy = Pharmacies.objects.all().order_by('id')
	pharmacies_page = Paginator(pharmacy, show_pharmacy_by)
	try:
		args['pharmacies_list'] = pharmacies_list = pharmacies_page.page(page_number)
	except:
		args['pharmacies_list'] = pharmacies_list = pharmacies_page.page(1)
	
	args['pharmacies_has_address'] = pharmacies_has_address = PharmaciesHasAddresses.objects.all()
	args['cities'] = cities = Cities.objects.all()
	args['pharmacies_has_contact'] = pharmacies_has_contact = PharmaciesHasContactinfos.objects.all()
	args['contact_types'] = contact_types = Contacttypes.objects.all()
	
	index = pharmacies_list.number - 1
	max_index = len(pharmacies_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(pharmacies_page.page_range)[start_index:end_index]
	return render(request, 'pharmacy_manager.html', args)


@user_login_required
def pharmacies_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/pharmacy/')
			response.set_cookie('show_pharmacy_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/pharmacy/')
	else:
		return HttpResponseRedirect('/pharmacy/')


@user_login_required
def pharmacy_add(request):
	if request.POST:
		try:
			pharmacy = Pharmacies.objects.get(
				id__exact=request.POST.get('pharmacy_id')
			)
		except:
			pharmacy = None
		
		if pharmacy is not None:
			pharmacy.name = request.POST.get('pharmacy_name')
			pharmacy.description = request.POST.get('pharmacy_description')
			pharmacy.save()
			return HttpResponseRedirect('/pharmacy/')
		else:
			pharmacy_save_form = PharmacyForm(data=request.POST)
			if pharmacy_save_form.is_valid():
				pharmacy = pharmacy_save_form.save(commit=False)
				pharmacy.name = request.POST.get('pharmacy_name')
				pharmacy.description = request.POST.get('pharmacy_description')
				pharmacy.save()
				return HttpResponseRedirect('/pharmacy/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/pharmacy/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/pharmacy/' + message + msg_txt)


@user_login_required
def pharmacy_delete(request):
	if request.POST:
		try:
			pharmacy_id = request.POST.get('pharmacy-delete')
			pharmacy = Pharmacies.objects.get(id=pharmacy_id)
			pharmacy.delete()
			return HttpResponseRedirect('/pharmacy/')
		except ObjectDoesNotExist:
			pharmacy = None
			return HttpResponseRedirect('/pharmacy/')
	else:
		return HttpResponseRedirect('/pharmacy/')


@user_login_required
def pharmacy_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('pharmacy_search')
		if search_query != "":
			pharmacy_name = Pharmacies.objects.filter(
				name__icontains=str(request.POST.get('pharmacy_search'))
			).order_by('id')
			pharmacy_description = Pharmacies.objects.filter(
				description__icontains=str(request.POST.get('pharmacy_search'))
			).exclude(
				id__in=pharmacy_name.values_list('id'),
			)
			
			args['all_pharmacies'] = all_pharmacies = list(
				chain(
					pharmacy_name,
					pharmacy_description,
				)
			)

			page_number = request.POST.get('page_number', "1")
			pharmacies_page = Paginator(all_pharmacies, 20)
			
			try:
				args['pharmacies_list_search'] = pharmacies_list = pharmacies_page.page(page_number)
			except:
				args['pharmacies_list_search'] = pharmacies_list = pharmacies_page.page(1)

			args['pharmacies_has_address'] = pharmacies_has_address = PharmaciesHasAddresses.objects.all()
			args['cities'] = cities = Cities.objects.all()
			args['pharmacies_has_contact'] = pharmacies_has_contact = PharmaciesHasContactinfos.objects.all()
			args['contact_types'] = contact_types = Contacttypes.objects.all()
			
			index = pharmacies_list.number - 1
			max_index = len(pharmacies_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(pharmacies_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('pharmacy_search')
			
			return render(request, "tables/pharmacies_tables/pharmacy_table_search.html", args)
		else:
			return HttpResponseRedirect("/pharmacy/")
	else:
		return HttpResponseRedirect("/pharmacy/")


@user_login_required
def pharmacy_address_edit(request):
	args['cities'] = cities = Cities.objects.all()
	args['address'] = Addresses.objects.get(id=request.POST.get('address_id'))
	return render(request, "tables/pharmacies_tables/pharmacy_address_edit.html", args)


@user_login_required
def pharmacy_address_cancel(request):
	args['cities'] = cities = Cities.objects.all()
	args['address'] = Addresses.objects.get(id=request.POST.get('address_id'))
	return render(request, "tables/pharmacies_tables/pharmacy_address_cancel.html", args)


@user_login_required
def pharmacy_contact_edit(request):
	args['contact'] = Contactinfos.objects.get(id=request.POST.get('contact_id'))
	return render(request, "tables/pharmacies_tables/pharmacy_contact_edit.html", args)


@user_login_required
def pharmacy_contact_cancel(request):
	args['contact'] = Contactinfos.objects.get(id=request.POST.get('contact_id'))
	return render(request, "tables/pharmacies_tables/pharmacy_contact_cancel.html", args)

	
@user_login_required
def product_brands_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_product_brands_by', ''))) >= 1:
		show_product_brands_by = request.COOKIES.get('show_product_brands_by')
	else:
		show_product_brands_by = 20
	
	args['product_brands'] = product_brands = Productbrands.objects.all().order_by('name')
	product_brands_page = Paginator(product_brands, show_product_brands_by)
	try:
		args['product_brands_list'] = product_brands_list = product_brands_page.page(page_number)
	except:
		args['product_brands_list'] = product_brands_list = product_brands_page.page(1)
	index = product_brands_list.number - 1
	max_index = len(product_brands_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(product_brands_page.page_range)[start_index:end_index]
	return render(request, 'product_brands_manager.html', args)


@user_login_required
def product_brands_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/product-brands/')
			response.set_cookie('show_product_brands_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/product-brands/')
	else:
		return HttpResponseRedirect('/product-brands/')


@user_login_required
def product_brands_add(request):
	if request.POST:
		try:
			product_brands = Productbrands.objects.get(
				id__exact=request.POST.get('product_brands_id')
			)
		except:
			product_brands = None
		
		if product_brands is not None:
			product_brands.name = request.POST.get('product_brands_name')
			product_brands.save()
			return HttpResponseRedirect('/product-brands/')
		else:
			product_brands_save_form = ProductbrandsForm(data=request.POST)
			if product_brands_save_form.is_valid():
				product_brands = product_brands_save_form.save(commit=False)
				product_brands.name = request.POST.get('product_brands_name')
				product_brands.save()
				return HttpResponseRedirect('/product-brands/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/product-brands/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/product-brands/' + message + msg_txt)


@user_login_required
def product_brands_delete(request):
	if request.POST:
		try:
			product_brands_id = request.POST.get('product-brands-delete')
			product_brands = Productbrands.objects.get(id=product_brands_id)
			product_brands.delete()
			return HttpResponseRedirect('/product-brands/')
		except ObjectDoesNotExist:
			product = None
			return HttpResponseRedirect('/product-brands/')
	else:
		return HttpResponseRedirect('/product-brands/')


@user_login_required
def product_brands_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('product_brands_search')
		if search_query != "":
			product_brands_name = Productbrands.objects.filter(
				name__icontains=str(request.POST.get('product_brands_search'))
			).order_by('name')
			
			args['all_product_brands'] = all_product_brands = list(
				chain(
					product_brands_name,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			product_brands_page = Paginator(all_product_brands, 20)
			
			try:
				args['product_brands_list_search'] = product_brands_list = product_brands_page.page(page_number)
			except:
				args['product_brands_list_search'] = product_brands_list = product_brands_page.page(1)
			index = product_brands_list.number - 1
			max_index = len(product_brands_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(product_brands_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('product_brands_search')
			
			return render(request, "tables/product_brands_tables/product_brands_table_search.html", args)
		else:
			return HttpResponseRedirect("/product-brands/")
	else:
		return HttpResponseRedirect("/product-brands/")
	

@user_login_required
def product_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_product_by', ''))) >= 1:
		show_product_by = request.COOKIES.get('show_product_by')
	else:
		show_product_by = 20
	
	args['products'] = products = Products.objects.all().order_by('name')
	args['product_brands'] = Productbrands.objects.all().order_by('name')
	product_page = Paginator(products, show_product_by)
	try:
		args['product_list'] = product_list = product_page.page(page_number)
	except:
		args['product_list'] = product_list = product_page.page(1)
	index = product_list.number - 1
	max_index = len(product_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(product_page.page_range)[start_index:end_index]
	return render(request, 'products_manager.html', args)


@user_login_required
def product_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/product/')
			response.set_cookie('show_product_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/product/')
	else:
		return HttpResponseRedirect('/product/')


@user_login_required
def product_add(request):
	if request.POST:
		try:
			product = Products.objects.get(
				id__exact=request.POST.get('product_id')
			)
		except:
			product = None
		
		if product is not None:
			product.barcode = request.POST.get('product_barcode')
			product.name = request.POST.get('product_name')
			product.description = request.POST.get('product_description')
			product.active = request.POST.get('product_active')
			product.productbrands_id = request.POST.get('response_edit_product_brand_id')
			product.save()
			return HttpResponseRedirect('/product/')
		else:
			product_save_form = ProductForm(data=request.POST)
			if product_save_form.is_valid():
				product = product_save_form.save(commit=False)
				product.barcode = request.POST.get('product_barcode')
				product.name = request.POST.get('product_name')
				product.description = request.POST.get('product_description')
				if request.POST.get('product_active') == 'on':
					product.active = 1
				else:
					product.active = 0
				product.productbrands_id = request.POST.get('response_product_brand_id')
				product.save()
				return HttpResponseRedirect('/product/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/product/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/product/' + message + msg_txt)


@user_login_required
def product_delete(request):
	if request.POST:
		try:
			product_id = request.POST.get('product-delete')
			product = Products.objects.get(id=product_id)
			product.delete()
			return HttpResponseRedirect('/product/')
		except ObjectDoesNotExist:
			product = None
			return HttpResponseRedirect('/product/')
	else:
		return HttpResponseRedirect('/product/')


@user_login_required
def product_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('product_search')
		if search_query != "":
			product_name = Products.objects.filter(
				name__icontains=str(request.POST.get('product_search'))
			).order_by('name')
			product_description = Products.objects.filter(
				description__icontains=str(request.POST.get('product_search'))
			).exclude(
				id__in=product_name.values_list('id'),
			)
			
			args['all_products'] = all_products = list(
				chain(
					product_name,
					product_description,
				)
			)
			
			# args['products'] = products = Products.objects.all().order_by('name')
			# args['product_brands'] = Productbrands.objects.all().order_by('name')
			
			page_number = request.POST.get('page_number', "1")
			product_page = Paginator(all_products, 20)
			
			try:
				args['product_list_search'] = product_list = product_page.page(page_number)
			except:
				args['product_list_search'] = product_list = product_page.page(1)
			index = product_list.number - 1
			max_index = len(product_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(product_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('product_search')
			
			return render(request, "tables/products_tables/product_table_search.html", args)
		else:
			return HttpResponseRedirect("/product/")
	else:
		return HttpResponseRedirect("/product/")


@user_login_required
def product_ajax(request):
	args['product_brands'] = product_brands = Productbrands.objects.filter(
		name__icontains=request.POST.get("product-brand-name"))[:5]
	return render(request, "tables/products_tables/product_ajax.html", args)


@user_login_required
def product_ajax_edit(request):
	args['product_brands'] = product_brands = Productbrands.objects.filter(
		name__icontains=request.POST.get("product-brand-name"))[:5]
	args['product'] = product = Products.objects.get(id__exact=request.POST.get("product"))
	return render(request, "tables/products_tables/product_ajax_edit.html", args)


@user_login_required
def city_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_city_by', ''))) >= 1:
		show_city_by = request.COOKIES.get('show_city_by')
	else:
		show_city_by = 20
	
	args['city'] = city = Cities.objects.all().order_by('id')
	city_page = Paginator(city, show_city_by)
	try:
		args['city_list'] = city_list = city_page.page(page_number)
	except:
		args['city_list'] = city_list = city_page.page(1)
	index = city_list.number - 1
	max_index = len(city_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(city_page.page_range)[start_index:end_index]
	return render(request, 'cities_manager.html', args)


@user_login_required
def city_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/city/')
			response.set_cookie('show_city_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/city/')
	else:
		return HttpResponseRedirect('/city/')


@user_login_required
def city_add(request):
	if request.POST:
		try:
			city = Cities.objects.get(
				id__exact=request.POST.get('city_id')
			)
		except:
			city = None
		
		if city is not None:
			city.city = request.POST.get('city_name')
			city.save()
			return HttpResponseRedirect('/city/')
		else:
			city_save_form = CityForm(data=request.POST)
			if city_save_form.is_valid():
				city = city_save_form.save(commit=False)
				city.city = request.POST.get('city_name')
				city.save()
				return HttpResponseRedirect('/city/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/city/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/city/' + message + msg_txt)


@user_login_required
def city_delete(request):
	if request.POST:
		try:
			city_id = request.POST.get('city-delete')
			city = Cities.objects.get(id=city_id)
			city.delete()
			return HttpResponseRedirect('/city/')
		except ObjectDoesNotExist:
			city = None
			return HttpResponseRedirect('/city/')
	else:
		return HttpResponseRedirect('/city/')


@user_login_required
def county_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_county_by', ''))) >= 1:
		show_county_by = request.COOKIES.get('show_county_by')
	else:
		show_county_by = 20
	
	args['county'] = county = Counties.objects.all().order_by('county')
	args['cities'] = cities = Cities.objects.all().order_by('id')
	county_page = Paginator(county, show_county_by)
	try:
		args['county_list'] = county_list = county_page.page(page_number)
	except:
		args['county_list'] = county_list = county_page.page(1)
	index = county_list.number - 1
	max_index = len(county_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(county_page.page_range)[start_index:end_index]
	return render(request, 'counties_manager.html', args)


@user_login_required
def county_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/county/')
			response.set_cookie('show_county_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/county/')
	else:
		return HttpResponseRedirect('/county/')


@user_login_required
def county_add(request):
	if request.POST:
		try:
			county = Counties.objects.get(
				id__exact=request.POST.get('county_id')
			)
		except:
			county = None
		
		if county is not None:
			county.county = request.POST.get('county_name')
			county.cities_id = request.POST.get('county_cities')
			county.save()
			return HttpResponseRedirect('/county/')
		else:
			county_save_form = CountyForm(data=request.POST)
			if county_save_form.is_valid():
				county = county_save_form.save(commit=False)
				county.county = request.POST.get('county_name')
				county.cities_id = request.POST.get('county_cities')
				county.save()
				return HttpResponseRedirect('/county/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/county/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/county/' + message + msg_txt)


@user_login_required
def county_delete(request):
	if request.POST:
		try:
			county_id = request.POST.get('county-delete')
			county = Counties.objects.get(id=county_id)
			county.delete()
			return HttpResponseRedirect('/county/')
		except ObjectDoesNotExist:
			county = None
			return HttpResponseRedirect('/county/')
	else:
		return HttpResponseRedirect('/county/')


@user_login_required
def district_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_county_by', ''))) >= 1:
		show_district_by = request.COOKIES.get('show_district_by')
	else:
		show_district_by = 20
	
	args['district'] = district = Districts.objects.all().order_by('district')
	args['cities'] = cities = Cities.objects.all()
	district_page = Paginator(district, show_district_by)
	try:
		args['district_list'] = district_list = district_page.page(page_number)
	except:
		args['district_list'] = district_list = district_page.page(1)
	index = district_list.number - 1
	max_index = len(district_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(district_page.page_range)[start_index:end_index]
	return render(request, 'districts_manager.html', args)


@user_login_required
def load_counties(request):
	args['city_id'] = city_id = request.GET.get('counties_city_name')
	args['counties'] = county = Counties.objects.filter(cities_id=city_id).order_by('county')
	return render(request, 'tables/districts_tables/district_ajax.html', args)


@user_login_required
def district_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/district/')
			response.set_cookie('show_district_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/district/')
	else:
		return HttpResponseRedirect('/district/')


@user_login_required
def district_add(request):
	if request.POST:
		try:
			district = Districts.objects.get(
				id__exact=request.POST.get('district_id')
			)
		except:
			district = None
		
		if district is not None:
			district.district = request.POST.get('district_name')
			district.counties_id = request.POST.get('county_name')
			district.counties_cities_id = request.POST.get('counties_city_name')
			district.save()
			return HttpResponseRedirect('/district/')
		else:
			district_save_form = DistrictForm(data=request.POST)
			if district_save_form.is_valid():
				district = district_save_form.save(commit=False)
				district.district = request.POST.get('district_name')
				district.counties_id = request.POST.get('county_name')
				district.counties_cities_id = request.POST.get('counties_city_name')
				district.save()
				return HttpResponseRedirect('/district/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/district/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/district/' + message + msg_txt)


@user_login_required
def district_delete(request):
	if request.POST:
		try:
			district_id = request.POST.get('district-delete')
			district = Districts.objects.get(id=district_id)
			district.delete()
			return HttpResponseRedirect('/district/')
		except ObjectDoesNotExist:
			district = None
			return HttpResponseRedirect('/district/')
	else:
		return HttpResponseRedirect('/district/')


@user_login_required
def person_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_person_by', ''))) >= 1:
		show_person_by = request.COOKIES.get('show_person_by')
	else:
		show_person_by = 20
	
	args['person'] = person = Persons.objects.all().order_by('id')
	args['persons_has_address'] = persons_has_address = PersonsHasAddresses.objects.all()
	args['cities'] = cities = Cities.objects.all()
	args['persons_has_contact'] = persons_has_contact = PersonsHasContactinfos.objects.all()
	args['contact_types'] = contact_types = Contacttypes.objects.all()
	persons_page = Paginator(person, show_person_by)
	try:
		args['persons_list'] = persons_list = persons_page.page(page_number)
	except:
		args['persons_list'] = persons_list = persons_page.page(1)
	index = persons_list.number - 1
	max_index = len(persons_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(persons_page.page_range)[start_index:end_index]
	return render(request, 'persons_manager.html', args)


@user_login_required
def person_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/person/')
			response.set_cookie('show_person_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/person/')
	else:
		return HttpResponseRedirect('/person/')


@user_login_required
def person_add(request):
	if request.POST:
		try:
			person = Persons.objects.get(
				id__exact=request.POST.get('person_id')
			)
		except:
			person = None
		
		if person is not None:
			person.name = request.POST.get('person_name')
			person.surname = request.POST.get('person_surname')
			person.username = request.POST.get('person_username')
			person.gender = request.POST.get('person_gender')
			password = hashlib.md5(request.POST.get('person_password').encode())
			person.password = password.hexdigest().upper()
			person.admin = request.POST.get('person_admin')
			person.active = request.POST.get('person_active')
			person.save()
			return HttpResponseRedirect('/person/')
		else:
			person_save_form = PersonForm(data=request.POST)
			if person_save_form.is_valid():
				person = person_save_form.save(commit=False)
				person.name = request.POST.get('person_name')
				person.surname = request.POST.get('person_surname')
				person.username = request.POST.get('person_username')
				person.gender = request.POST.get('person_gender')
				password = hashlib.md5(request.POST.get('person_password').encode())
				person.password = password.hexdigest().upper()
				person.admin = request.POST.get('person_admin')
				if request.POST.get('person_active') == 'on':
					person.active = 1
				else:
					person.active = 0
				person.save()
				return HttpResponseRedirect('/person/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/person/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/person/' + message + msg_txt)


@user_login_required
def person_delete(request):
	if request.POST:
		try:
			person_id = request.POST.get('person-delete')
			person = Persons.objects.get(id=person_id)
			person.delete()
			return HttpResponseRedirect('/person/')
		except ObjectDoesNotExist:
			person = None
			return HttpResponseRedirect('/person/')
	else:
		return HttpResponseRedirect('/person/')


@user_login_required
def person_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('person_search')
		if search_query != "":
			person_name = Persons.objects.filter(
				name__icontains=str(request.POST.get('person_search'))
			).order_by('name')
			person_surname = Persons.objects.filter(
				surname__icontains=str(request.POST.get('person_search'))
			).exclude(
				id__in=person_name.values_list('id'),
			)
			person_username = Persons.objects.filter(
				username__icontains=str(request.POST.get('person_search'))
			).exclude(
				id__in=person_name.values_list('id'),
			)
			args['all_persons'] = all_persons = list(
				chain(
					person_name,
					person_surname,
					person_username,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			person_page = Paginator(all_persons, 20)
			
			try:
				args['person_list_search'] = person_list = person_page.page(page_number)
			except:
				args['person_list_search'] = person_list = person_page.page(1)
			index = person_list.number - 1
			max_index = len(person_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(person_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('person_search')
			
			return render(request, "tables/persons_tables/person_table_search.html", args)
		else:
			return HttpResponseRedirect("/person/")
	else:
		return HttpResponseRedirect("/person/")
	
	
@user_login_required
def person_address_edit(request):
	args['cities'] = cities = Cities.objects.all()
	args['address'] = Addresses.objects.get(id=request.POST.get('address_id'))
	return render(request, "tables/persons_tables/person_address_edit.html", args)


@user_login_required
def person_address_cancel(request):
	args['cities'] = cities = Cities.objects.all()
	args['address'] = Addresses.objects.get(id=request.POST.get('address_id'))
	return render(request, "tables/persons_tables/person_address_cancel.html", args)


@user_login_required
def person_contact_edit(request):
	args['contact'] = Contactinfos.objects.get(id=request.POST.get('contact_id'))
	return render(request, "tables/persons_tables/person_contact_edit.html", args)


@user_login_required
def person_contact_cancel(request):
	args['contact'] = Contactinfos.objects.get(id=request.POST.get('contact_id'))
	return render(request, "tables/persons_tables/person_contact_cancel.html", args)
	
	
@user_login_required
def address_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_address_by', ''))) >= 1:
		show_address_by = request.COOKIES.get('show_address_by')
	else:
		show_address_by = 20
	
	args['address'] = address = Addresses.objects.all().order_by('id')
	args['cities'] = cities = Cities.objects.all()
	addresses_page = Paginator(address, show_address_by)
	try:
		args['addresses_list'] = addresses_list = addresses_page.page(page_number)
	except:
		args['addresses_list'] = addresses_list = addresses_page.page(1)
	index = addresses_list.number - 1
	max_index = len(addresses_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(addresses_page.page_range)[start_index:end_index]
	return render(request, 'addresses_manager.html', args)


@user_login_required
def address_load_counties(request):
	args['city_id'] = city_id = request.GET.get('address_cities')
	args['counties'] = county = Counties.objects.filter(cities_id=city_id).order_by('county')
	return render(request, 'tables/addresses_tables/address_counties_ajax.html', args)


@user_login_required
def address_load_districts(request):
	args['county_id'] = county_id = request.GET.get('address_counties')
	args['districts'] = districts = Districts.objects.filter(counties=county_id).order_by('district')
	return render(request, 'tables/addresses_tables/address_districts_ajax.html', args)


@user_login_required
def address_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/address/')
			response.set_cookie('show_address_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/address/')
	else:
		return HttpResponseRedirect('/address/')


@user_login_required
def address_add(request):
	if request.POST:
		try:
			address = Addresses.objects.get(
				id__exact=request.POST.get('address_id')
			)
		except:
			address = None
		
		if address is not None:
			address.description = request.POST.get('address_description')
			address.otheraddrinfo = request.POST.get('address_otheraddrinfo')
			address.active = request.POST.get('address_active')
			address.districts_id = request.POST.get('address_districts')
			address.counties_id = request.POST.get('address_counties')
			address.cities_id = request.POST.get('address_cities')
			address.save()
			return HttpResponseRedirect('/address/')
		else:
			address_save_form = AddressForm(data=request.POST)
			if address_save_form.is_valid():
				address = address_save_form.save(commit=False)
				address.description = request.POST.get('address_description')
				address.otheraddrinfo = request.POST.get('address_otheraddrinfo')
				if request.POST.get('address_active') == 'on':
					address.active = 1
				else:
					address.active = 0
				address.districts_id = request.POST.get('address_districts')
				address.counties_id = request.POST.get('address_counties')
				address.cities_id = request.POST.get('address_cities')
				address.save()
				return HttpResponseRedirect('/address/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/address/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/address/' + message + msg_txt)


@user_login_required
def address_delete(request):
	if request.POST:
		try:
			address_id = request.POST.get('address-delete')
			address = Addresses.objects.get(id=address_id)
			address.delete()
			return HttpResponseRedirect('/address/')
		except ObjectDoesNotExist:
			address = None
			return HttpResponseRedirect('/address/')
	else:
		return HttpResponseRedirect('/address/')


@user_login_required
def contact_types_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_contact_types_by', ''))) >= 1:
		show_contact_types_by = request.COOKIES.get('show_contact_types_by')
	else:
		show_contact_types_by = 20
	
	args['contact_types'] = contact_types = Contacttypes.objects.all().order_by('id')
	contact_types_page = Paginator(contact_types, show_contact_types_by)
	try:
		args['contact_types_list'] = contact_types_list = contact_types_page.page(page_number)
	except:
		args['contact_types_list'] = contact_types_list = contact_types_page.page(1)
	index = contact_types_list.number - 1
	max_index = len(contact_types_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(contact_types_page.page_range)[start_index:end_index]
	return render(request, 'contact_types_manager.html', args)


@user_login_required
def contact_types_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/contact-types/')
			response.set_cookie('show_contact_types_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/contact-types/')
	else:
		return HttpResponseRedirect('/contact-types/')


@user_login_required
def contact_types_add(request):
	if request.POST:
		try:
			contact_types = Contacttypes.objects.get(
				id__exact=request.POST.get('contact_types_id')
			)
		except:
			contact_types = None
		
		if contact_types is not None:
			contact_types.name = request.POST.get('contact_types_name')
			contact_types.save()
			return HttpResponseRedirect('/contact-types/')
		else:
			contact_types_save_form = ContacttypesForm(data=request.POST)
			if contact_types_save_form.is_valid():
				contact_types = contact_types_save_form.save(commit=False)
				contact_types.name = request.POST.get('contact_types_name')
				contact_types.save()
				return HttpResponseRedirect('/contact-types/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/contact-types/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/contact-types/' + message + msg_txt)


@user_login_required
def contact_types_delete(request):
	if request.POST:
		try:
			contact_types_id = request.POST.get('contact-types-delete')
			contact_types = Contacttypes.objects.get(id=contact_types_id)
			contact_types.delete()
			return HttpResponseRedirect('/contact-types/')
		except ObjectDoesNotExist:
			contact_types = None
			return HttpResponseRedirect('/contact-types/')
	else:
		return HttpResponseRedirect('/contact-types/')


@user_login_required
def contact_info_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_contact_info_by', ''))) >= 1:
		show_contact_info_by = request.COOKIES.get('show_contact_info_by')
	else:
		show_contact_info_by = 20
	
	args['contact_info'] = contact_info = Contactinfos.objects.all().order_by('id')
	args['contact_types'] = contact_types = Contacttypes.objects.all().order_by('id')
	contact_info_page = Paginator(contact_info, show_contact_info_by)
	try:
		args['contact_info_list'] = contact_info_list = contact_info_page.page(page_number)
	except:
		args['contact_info_list'] = contact_info_list = contact_info_page.page(1)
	index = contact_info_list.number - 1
	max_index = len(contact_info_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(contact_info_page.page_range)[start_index:end_index]
	return render(request, 'contact_infos_manager.html', args)


@user_login_required
def contact_info_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/contact-info/')
			response.set_cookie('show_contact_info_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/contact-info/')
	else:
		return HttpResponseRedirect('/contact-info/')


@user_login_required
def contact_info_add(request):
	if request.POST:
		try:
			contact_info = Contactinfos.objects.get(
				id__exact=request.POST.get('contact_info_id')
			)
		except:
			contact_info = None
		
		if contact_info is not None:
			contact_info.text = request.POST.get('contact_text')
			contact_info.active = request.POST.get('contact_active')
			contact_info.contacttypes_id = request.POST.get('contact_contacttypes')
			contact_info.save()
			return HttpResponseRedirect('/contact-info/')
		else:
			contact_info_save_form = ContactForm(data=request.POST)
			if contact_info_save_form.is_valid():
				contact_info = contact_info_save_form.save(commit=False)
				contact_info.text = request.POST.get('contact_text')
				if request.POST.get('contact_active') == 'on':
					contact_info.active = 1
				else:
					contact_info.active = 0
				contact_info.contacttypes_id = request.POST.get('contact_contacttypes')
				contact_info.save()
				return HttpResponseRedirect('/contact-info/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/contact-info/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/contact-info/' + message + msg_txt)


@user_login_required
def contact_info_delete(request):
	if request.POST:
		try:
			contact_info_id = request.POST.get('contact-info-delete')
			contact_info = Contactinfos.objects.get(id=contact_info_id)
			contact_info.delete()
			return HttpResponseRedirect('/contact-info/')
		except ObjectDoesNotExist:
			contact_info = None
			return HttpResponseRedirect('/contact-info/')
	else:
		return HttpResponseRedirect('/contact-info/')


@user_login_required
def product_buy_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_product_buy_by', ''))) >= 1:
		show_product_buy_by = request.COOKIES.get('show_product_buy_by')
	else:
		show_product_buy_by = 20
	
	args['product_buy'] = product_buy = Buyingproducts.objects.all().order_by('id')
	args['products'] = products = Products.objects.all().order_by('name')
	product_buy_page = Paginator(product_buy, show_product_buy_by)
	try:
		args['product_buy_list'] = product_buy_list = product_buy_page.page(page_number)
	except:
		args['product_buy_list'] = product_buy_list = product_buy_page.page(1)
	index = product_buy_list.number - 1
	max_index = len(product_buy_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(product_buy_page.page_range)[start_index:end_index]
	return render(request, 'product_buy_manager.html', args)


@user_login_required
def product_buy_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/product-buy/')
			response.set_cookie('show_product_buy_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/product-buy/')
	else:
		return HttpResponseRedirect('/product-buy/')


@user_login_required
def product_buy_add(request):
	if request.POST:
		try:
			product_buy = Buyingproducts.objects.get(
				id__exact=request.POST.get('product_buy_id')
			)
		except:
			product_buy = None
		
		if product_buy is not None:
			buydate_unformatted = datetime.datetime.strptime(request.POST.get('product_buy_buydate'), "%d.%m.%Y")
			product_buy.buydate = formats.date_format(buydate_unformatted, "Y-m-d")
			product_buy.description = request.POST.get('product_buy_description')
			product_buy.quantity = request.POST.get('product_buy_quantity')
			product_buy.products_id = request.POST.get('response_edit_product_id')
			product_buy.save()
			return HttpResponseRedirect('/product-buy/')
		else:
			product_buy_save_form = ProductBuyForm(data=request.POST)
			if product_buy_save_form.is_valid():
				product_buy = product_buy_save_form.save(commit=False)
				product_buy.buydate = request.POST.get('product_buy_buydate')
				product_buy.description = request.POST.get('product_buy_description')
				product_buy.quantity = request.POST.get('product_buy_quantity')
				product_buy.products_id = request.POST.get('response_product_id')
				product_buy.save()
				return HttpResponseRedirect('/product-buy/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/product-buy/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/product-buy/' + message + msg_txt)


@user_login_required
def product_buy_delete(request):
	if request.POST:
		try:
			product_buy_id = request.POST.get('product-buy-delete')
			product_buy = Buyingproducts.objects.get(id=product_buy_id)
			product_buy.delete()
			return HttpResponseRedirect('/product-buy/')
		except ObjectDoesNotExist:
			product_buy = None
			return HttpResponseRedirect('/product-buy/')
	else:
		return HttpResponseRedirect('/product-buy/')


@user_login_required
def product_buy_ajax(request):
	args['products'] = products = Products.objects.filter(name__icontains=request.POST.get("product-name"))[:5]
	return render(request, "tables/product_buy_tables/product_buy_ajax.html", args)


@user_login_required
def product_buy_ajax_edit(request):
	args['products'] = products = Products.objects.filter(name__icontains=request.POST.get("product-name"))[:5]
	args['product_buys'] = product_buys = Buyingproducts.objects.get(id__exact=request.POST.get("product-edit"))
	return render(request, "tables/product_buy_tables/product_buy_ajax_edit.html", args)


@user_login_required
def product_buy_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('product_buy_search')
		if search_query != "":
			product_buy_product_name = Buyingproducts.objects.filter(
				products__name__icontains=str(request.POST.get('product_buy_search'))
			).order_by('id')
			product_buy_description = Buyingproducts.objects.filter(
				description__icontains=str(request.POST.get('product_buy_search'))
			).exclude(
				id__in=product_buy_product_name.values_list('id'),
			)
			
			args['all_product_buys'] = all_product_buys = list(
				chain(
					product_buy_product_name,
					product_buy_description,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			product_buy_page = Paginator(all_product_buys, 20)
			
			try:
				args['product_buy_list_search'] = product_buy_list = product_buy_page.page(page_number)
			except:
				args['product_buy_list_search'] = product_buy_list = product_buy_page.page(1)
			index = product_buy_list.number - 1
			max_index = len(product_buy_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(product_buy_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('product_buy_search')
			
			return render(request, "tables/product_buy_tables/product_buy_table_search.html", args)
		else:
			return HttpResponseRedirect("/product-buy/")
	else:
		return HttpResponseRedirect("/product-buy/")


@user_login_required
def product_sale_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_product_sale_by', ''))) >= 1:
		show_product_sale_by = request.COOKIES.get('show_product_sale_by')
	else:
		show_product_sale_by = 20
	
	args['product_sale'] = product_sale = Productsales.objects.all().order_by('id')
	args['products'] = products = Products.objects.all().order_by('name')
	product_sale_page = Paginator(product_sale, show_product_sale_by)
	try:
		args['product_sale_list'] = product_sale_list = product_sale_page.page(page_number)
	except:
		args['product_sale_list'] = product_sale_list = product_sale_page.page(1)
	index = product_sale_list.number - 1
	max_index = len(product_sale_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(product_sale_page.page_range)[start_index:end_index]
	return render(request, 'product_sales_manager.html', args)


@user_login_required
def product_sale_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/product-sale/')
			response.set_cookie('show_product_sale_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/product-sale/')
	else:
		return HttpResponseRedirect('/product-sale/')


@user_login_required
def product_sale_add(request):
	if request.POST:
		try:
			product_sale = Productsales.objects.get(
				id__exact=request.POST.get('product_sale_id')
			)
		except:
			product_sale = None
		
		if product_sale is not None:
			saledate_unformatted = datetime.datetime.strptime(request.POST.get('product_sale_saledate'), "%d.%m.%Y")
			product_sale.saledate = formats.date_format(saledate_unformatted, "Y-m-d")
			product_sale.description = request.POST.get('product_sale_description')
			product_sale.quantity = request.POST.get('product_sale_quantity')
			product_sale.products_id = request.POST.get('response_edit_product_id')
			product_sale.pharmacies_id = request.POST.get('response_edit_pharmacy_id')
			product_sale.save()
			return HttpResponseRedirect('/product-sale/')
		else:
			product_sale_save_form = ProductSaleForm(data=request.POST)
			if product_sale_save_form.is_valid():
				product_sale = product_sale_save_form.save(commit=False)
				product_sale.saledate = request.POST.get('product_sale_saledate')
				product_sale.description = request.POST.get('product_sale_description')
				product_sale.quantity = request.POST.get('product_sale_quantity')
				product_sale.products_id = request.POST.get('response_product_id')
				product_sale.pharmacies_id = request.POST.get('response_pharmacy_id')
				product_sale.save()
				return HttpResponseRedirect('/product-sale/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/product-sale/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/product-sale/' + message + msg_txt)


@user_login_required
def product_sale_delete(request):
	if request.POST:
		try:
			product_sale_id = request.POST.get('product-sale-delete')
			product_sale = Productsales.objects.get(id=product_sale_id)
			product_sale.delete()
			return HttpResponseRedirect('/product-sale/')
		except ObjectDoesNotExist:
			product_sale = None
			return HttpResponseRedirect('/product-sale/')
	else:
		return HttpResponseRedirect('/product-sale/')


@user_login_required
def product_sale_ajax(request):
	args['products'] = products = Products.objects.filter(name__icontains=request.POST.get("product-name"))[:5]
	return render(request, "tables/product_sales_tables/product_sale_ajax.html", args)


@user_login_required
def product_sale_ajax_edit(request):
	args['products'] = products = Products.objects.filter(name__icontains=request.POST.get("product-name"))[:5]
	args['product_sales'] = product_sales = Productsales.objects.get(id__exact=request.POST.get("product-edit"))
	return render(request, "tables/product_sales_tables/product_sale_ajax_edit.html", args)


@user_login_required
def product_sale_ajax_pharmacy(request):
	args['pharmacies'] = pharmacies = Pharmacies.objects.filter(name__icontains=request.POST.get("pharmacy-name"))[:5]
	return render(request, "tables/product_sales_tables/product_sale_ajax_pharmacy.html", args)


@user_login_required
def product_sale_ajax_edit_pharmacy(request):
	args['pharmacies'] = pharmacies = Pharmacies.objects.filter(name__icontains=request.POST.get("pharmacy-name"))[:5]
	args['product_sales'] = product_sales = Productsales.objects.get(id__exact=request.POST.get("product-sales"))
	return render(request, "tables/product_sales_tables/product_sale_ajax_edit_pharmacy.html", args)


@user_login_required
def product_sale_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('product_sale_search')
		if search_query != "":
			product_sale_product_name = Productsales.objects.filter(
				products__name__icontains=str(request.POST.get('product_sale_search'))
			).order_by('id')
			product_sale_pharmacy_name = Productsales.objects.filter(
				pharmacies__name__icontains=str(request.POST.get('product_sale_search'))
			).exclude(
				id__in=product_sale_product_name.values_list('id'),
			)
			product_sale_description = Productsales.objects.filter(
				description__icontains=str(request.POST.get('product_sale_search'))
			).exclude(
				id__in=product_sale_product_name.values_list('id'),
			)
			
			args['all_product_sales'] = all_product_sales = list(
				chain(
					product_sale_product_name,
					product_sale_pharmacy_name,
					product_sale_description,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			product_sale_page = Paginator(all_product_sales, 20)
			
			try:
				args['product_sale_list_search'] = product_sale_list = product_sale_page.page(page_number)
			except:
				args['product_sale_list_search'] = product_sale_list = product_sale_page.page(1)
			index = product_sale_list.number - 1
			max_index = len(product_sale_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(product_sale_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('product_sale_search')
			
			return render(request, "tables/product_sales_tables/product_sales_table_search.html", args)
		else:
			return HttpResponseRedirect("/product-sale/")
	else:
		return HttpResponseRedirect("/product-sale/")
	
	
@user_login_required
def user_license_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_user_license_by', ''))) >= 1:
		show_user_license_by = request.COOKIES.get('show_user_license_by')
	else:
		show_user_license_by = 20
	
	args['user_license'] = user_license = Appuserlicenses.objects.all().order_by('id')
	args['persons'] = persons = Persons.objects.all().order_by('name')
	args['pharmacies'] = pharmacies = Pharmacies.objects.all()
	user_license_page = Paginator(user_license, show_user_license_by)
	try:
		args['user_license_list'] = user_license_list = user_license_page.page(page_number)
	except:
		args['user_license_list'] = user_license_list = user_license_page.page(1)
	index = user_license_list.number - 1
	max_index = len(user_license_page.page_range)
	start_index = index - 3 if index >= 3 else 0
	end_index = index + 3 if index <= max_index - 3 else max_index
	args['page_range'] = page_range = list(user_license_page.page_range)[start_index:end_index]
	return render(request, 'user_licenses_manager.html', args)


@user_login_required
def user_license_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/user-license/')
			response.set_cookie('show_user_license_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/user-license/')
	else:
		return HttpResponseRedirect('/user-license/')


@user_login_required
def user_license_add(request):
	if request.POST:
		try:
			user_license = Appuserlicenses.objects.get(
				id__exact=request.POST.get('user_license_id')
			)
		except:
			user_license = None
		
		if user_license is not None:
			startingdate_unformatted = datetime.datetime.strptime(request.POST.get('user_license_startingdate'), "%d.%m.%Y")
			user_license.startingdate = formats.date_format(startingdate_unformatted, "Y-m-d")
			enddate_unformatted = datetime.datetime.strptime(request.POST.get('user_license_enddate'), "%d.%m.%Y")
			user_license.enddate = formats.date_format(enddate_unformatted, "Y-m-d")
			user_license.active = request.POST.get('user_license_active')
			user_license.persons_id = request.POST.get('response_edit_person_id')
			user_license.pharmacies_id = request.POST.get('response_edit_pharmacy_id')
			user_license.save()
			return HttpResponseRedirect('/user-license/')
		else:
			user_license_save_form = UserLicenseForm(data=request.POST)
			if user_license_save_form.is_valid():
				user_license = user_license_save_form.save(commit=False)
				user_license.startingdate = request.POST.get('user_license_startingdate')
				user_license.enddate = request.POST.get('user_license_enddate')
				if request.POST.get('user_license_active') == 'on':
					user_license.active = 1
				else:
					user_license.active = 0
				user_license.persons_id = request.POST.get('response_person_id')
				user_license.pharmacies_id = request.POST.get('response_pharmacy_id')
				user_license.save()
				return HttpResponseRedirect('/user-license/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/user-license/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/user-license/' + message + msg_txt)


@user_login_required
def user_license_delete(request):
	if request.POST:
		try:
			user_license_id = request.POST.get('user-license-delete')
			user_license = Appuserlicenses.objects.get(id=user_license_id)
			user_license.delete()
			return HttpResponseRedirect('/user-license/')
		except ObjectDoesNotExist:
			user_license = None
			return HttpResponseRedirect('/user-license/')
	else:
		return HttpResponseRedirect('/user-license/')


@user_login_required
def user_license_ajax(request):
	args['persons'] = persons = Persons.objects.filter(username__icontains=request.POST.get("person-username"))[:5]
	return render(request, "tables/user_licenses_tables/user_license_ajax.html", args)


@user_login_required
def user_license_ajax_edit(request):
	args['persons'] = persons = Persons.objects.filter(username__icontains=request.POST.get("person-username"))[:5]
	args['user_license'] = user_license = Appuserlicenses.objects.get(id__exact=request.POST.get("user-license"))
	return render(request, "tables/user_licenses_tables/user_license_ajax_edit.html", args)


@user_login_required
def user_license_ajax_pharmacy(request):
	args['pharmacies'] = pharmacies = Pharmacies.objects.filter(name__icontains=request.POST.get("pharmacy-name"))[:5]
	return render(request, "tables/user_licenses_tables/user_license_ajax_pharmacy.html", args)


@user_login_required
def user_license_ajax_edit_pharmacy(request):
	args['pharmacies'] = pharmacies = Pharmacies.objects.filter(name__icontains=request.POST.get("pharmacy-name"))[:5]
	args['user_license'] = user_license = Appuserlicenses.objects.get(id__exact=request.POST.get("user-license"))
	return render(request, "tables/user_licenses_tables/user_license_ajax_edit_pharmacy.html", args)


@user_login_required
def user_license_search(request, page_number=1):
	if request.method == "POST":
		search_query = request.POST.get('user_license_search')
		if search_query != "":
			user_license_person_name = Appuserlicenses.objects.filter(
				persons__name__icontains=str(request.POST.get('user_license_search'))
			).order_by('id')
			user_license_person_surname = Appuserlicenses.objects.filter(
				persons__surname__icontains=str(request.POST.get('user_license_search'))
			).exclude(
				id__in=user_license_person_name.values_list('id'),
			)
			user_license_person_username = Appuserlicenses.objects.filter(
				persons__username__icontains=str(request.POST.get('user_license_search'))
			).exclude(
				id__in=user_license_person_name.values_list('id'),
			)
			user_license_pharmacy_name = Appuserlicenses.objects.filter(
				pharmacies__name__icontains=str(request.POST.get('user_license_search'))
			).exclude(
				id__in=user_license_person_name.values_list('id'),
			)
			
			args['all_user_licenses'] = all_user_licenses = list(
				chain(
					user_license_person_name,
					user_license_person_surname,
					user_license_person_username,
					user_license_pharmacy_name,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			user_license_page = Paginator(all_user_licenses, 20)
			
			try:
				args['user_license_list_search'] = user_license_list = user_license_page.page(page_number)
			except:
				args['user_license_list_search'] = user_license_list = user_license_page.page(1)
			index = user_license_list.number - 1
			max_index = len(user_license_page.page_range)
			start_index = index - 3 if index >= 3 else 0
			end_index = index + 3 if index <= max_index - 3 else max_index
			args['page_range'] = page_range = list(user_license_page.page_range)[start_index:end_index]
			args['search'] = search_query = request.POST.get('user_license_search')
			
			return render(request, "tables/user_licenses_tables/user_license_table_search.html", args)
		else:
			return HttpResponseRedirect("/user-license/")
	else:
		return HttpResponseRedirect("/user-license/")